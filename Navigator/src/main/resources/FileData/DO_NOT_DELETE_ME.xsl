<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4portrait" page-height="29.7cm" page-width="21cm" font-family="sans-serif" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
          <fo:region-body margin-top="1.0cm" margin-bottom="1cm"/>
          <fo:region-before extent="2cm" region-name="header"/>
          <fo:region-after extent="1.5cm" region-name="footer"/>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4portrait">
        <fo:static-content flow-name="header">
          <fo:block>
            Evaluation of bridge restrictions - National Highway Company, Department of Bridges
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="footer">
          <fo:block>
            <fo:page-number/>
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block>
            Company Name: <xsl:value-of select="document/about/company"/>
           </fo:block>
          <fo:block>
            Date of transport: <xsl:value-of select="document/about/date"/>
          </fo:block>
          <fo:block color="white">a</fo:block>
          <fo:block color="white">a</fo:block>
          <fo:block>MOSTY:</fo:block>
          <fo:block color="white">a</fo:block>
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="50mm"/>
            <fo:table-column column-width="100mm"/>
            <fo:table-header>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block>Mode</fo:block>
                  <fo:block color="white">a</fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block>Obstacle</fo:block>
                  <fo:block color="white">a</fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-header>
            <fo:table-body>
              <xsl:for-each select="document/trip/route">
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      Normal
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="normal"/><fo:block color="white">a</fo:block>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      Exclusive
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="exclusive"/><fo:block color="white">a</fo:block>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      Extraordinary
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="extraordinary"/><fo:block color="white">a</fo:block>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell>
                    <fo:block>
                      Unknown
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:value-of select="unknown"/><fo:block color="white">a</fo:block>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </xsl:for-each>
            </fo:table-body>
          </fo:table>
          <fo:block color="white">a</fo:block><fo:block color="white">a</fo:block>
          <fo:block>
            V Bratislave dna: __________________________
           </fo:block>
          <fo:block>
            Vystavil: __________________________ 
          </fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>
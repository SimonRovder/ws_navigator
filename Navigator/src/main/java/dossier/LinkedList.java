package dossier;

/**
 * This class handles all functions needed to operate a two-way linked list of Nodes
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
/*
 * MASTERY FACTOR 12-15 - Implementing abstract data types - Linked List.
 */
public class LinkedList {
	
	protected Node first; // First Node in the linked list
	protected Node last; // Last Node in the linked list
	protected int amount;
	
	/**
	 * Constructor sets the object variables to initial values
	 */
	public LinkedList(){
		this.nullify();
	}
	
	/**
	 * Sets the object variables to initial values
	 */
	public void nullify(){
		// Set all object variables to values indicating the linked list is empty:
		this.first = null; 	// No first Node
		this.last = null;	// No last Node
		this.amount = 0;	// Zero Node count
	}
	
	/**
	 * Returns the node that is at the position in the linked list.
	 * @param num The position of the required Node
	 * @return The required Node.
	 */
/*
 * MASTERY FACTOR 11 -- 12 - Searching.
 */
	public Node getNode(int num){
		long loop = 0;
		
		// If the requested position is negative, return null. 
		if(num < 0) return null;
		
		// Start at the first Node
		Node searcher = this.first;
		while(loop != num && searcher != null){
			// Switch to the next node as many times as the provided position requires
			loop++;
			searcher = searcher.getNext();
		}
		if(num == loop){
			return searcher; // Return the found Node
		}
		return null; // Return null if the Node position was out of the range of the list
	}
	
	/**
	 * Adds a Node to the end of the linked list.
	 * @param node Node that is to be added.
	 */
	public void addToEnd(Node node){
		node.setNext(null);
		node.setPrevious(null);

/*
 * MASTERY FACTOR 11 -- 4 - Simple selection.
 */
		if(this.last == null){
			// If the last Node is null, the provided Node is the first node.
			this.first = node;
		}else{
			// If the last node is not null, connect it with the provided node by pointers.
			this.last.setNext(node);
			node.setPrevious(this.last);
		}
		// Set the provided node as the last Node.
		this.last = node;
		
		// Increase the Node count.
		this.amount++;
	}
	
	/**
	 * Deletes the provided node from the linked list
	 * @param node Node to be deleted
	 */
	public void delete(Node node){
		if(node.getNext() == null && node.getPrevious() == null){
			/*
			 * If this Node is not connected to any other Node, the list will be empty after
			 * it is deleted. So the list has to be nullified
			 */
    		this.nullify();
    		return;
    	}
    	if(node.getNext() == null){
    		/*
    		 * If the pointer to the next Node is null, it is the last Node in the linked list.
    		 * So its previous Node has to be set as the last Node and they have to be disconnected 
    		 */
    		this.last = node.getPrevious();
    		this.last.setNext(null);
    		this.amount--; // Decrease the Node count
    		return;
    	}
    	if(node.getPrevious() == null){
    		/*
    		 * If the pointer to the previous Node is null, it is the first Node in the linked list.
    		 * So its next Node has to be set as the first Node and they have to be disconnected 
    		 */
    		this.first = node.getNext();
    		this.first.setPrevious(null);
    		this.amount--; // Decrease the Node count
    		return;
    	}
    	
    	/*
    	 * If neither of the above conditions were met, the Node is somewhere in the middle of the list,
    	 * surrounded by other Nodes. In such case, its next and previous Nodes have to be connected,
    	 * leaving the provided note out.
    	 */
    	node.getPrevious().setNext(node.getNext());
    	node.getNext().setPrevious(node.getPrevious());
    	this.amount--; // Decrease the Node count
	}
	
	/**
	 * Adds the provided node to the provided position
	 * @param node Node that is to be added.
	 * @param index Position to which the provided Node is to be added.
	 */
	public void addToPos(Node node, long index){
		Node o = this.first; // Start searching the linked list at the first node
/*
 * MASTERY FACTOR 11 -- 6 - Loops.
 */
		while(o != null && index > 0){
			// Iterate until the desired index is reached
			// Keep track of the previous Node, by setting the provided Node's previous Node as it.
			node.setPrevious(o); 
			o = o.getNext();
			index--;
		}
		if(o == null){
			// if the searching node reached null, the provided Node needs to be added to the end.
			this.addToEnd(node);
			/*
			 * The method addToEnd() increases the Node count, but this method will increase the
			 * Node count as well. So to only increase the Node count by one, the Node count must
			 * be decreased by one at this point.
			 */
			this.amount--;
		}else{
			if(o.getPrevious() == null){
				/* 
				 * If the searching Node's previous Node is null, the provided Node needs to be added
				 * to the beginning of the list. So the two need to be connected (provided Node being
				 * the previous)
				 */
				node.setNext(o);
				o.setPrevious(node);
				// And the provided Node needs to be set as the first Node
				this.first = node;
			}else{
				/*
		    	 * If neither of the above conditions were met, the provided Node's position is somewhere in
		    	 * the middle of the list, surrounded by other Nodes. In such case, the provided Node needs
		    	 * to be connected to them and they to it in such a way, that it is in between the two nodes
		    	 * surrounding the position.
		    	 */
				node.setNext(o);
				node.setPrevious(o.getPrevious());
				node.getNext().setPrevious(node);
				node.getPrevious().setNext(node);
			}
		}
		this.amount++; // Increase the Node count
	}
	
	/**
	 * Getter for the first Node in the list.
	 * @return The first Node in the list.
	 */
	protected Node getFirst() {
		return this.first;
	}

	/**
	 * Getter for the last Node in the list.
	 * @return The last Node in the list.
	 */
	protected Node getLast() {
		return this.last;
	}
	
	/**
	 * Getter for the amount of Nodes in the list.
	 * @return The amount of Nodes in the list.
	 */
	protected int getAmount(){
		return this.amount;
	}
}
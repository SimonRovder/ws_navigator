package dossier;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;


/**
 * Handles the GUI used to set up a single Route on a single Highway and contains the
 * algorithm for transforming the provided information via the GUI into the Route.
 * 
 * Calls the Editor class that created it to save the Route to the Trip in Guru
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class RoutePlannerPanel extends BasePanel implements ActionListener{
	private static final long serialVersionUID = 1588676795443677245L;
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton setEntry;
	private MyButton setExit;
	private JCheckBox useEntryBranch;
	private JCheckBox useExitBranch;
	private JComboBox entryBranches;
	private JComboBox exitBranches;
	private MyButton saveRoute;
	private MyTextField entry;
	private MyTextField exit;
	private JScrollPane scroller;
	private MyTable obstacles;
	
	/*
	 * Entry and exit Obstacles
	 */
	private Obstacle startObstacle;
	private Obstacle endObstacle;
	
	// The highway this RoutePlannerPanel deals with
	private Highway highway;
	
	// Pointer to the RoutePlanner that created this class (used to call its methods)
	private RoutePlanner parent;
	
	/**
	 * Constructor. Sets up the GUI and object variables
	 * @param highway
	 * @param routePlanner
	 */
	public RoutePlannerPanel(Highway highway, RoutePlanner routePlanner){
		super();					// Call to the constructor of the superclass
		
		// Set the object variables to the pointers passed in the parameters.
		this.highway = highway; 
		this.parent = routePlanner;
		
		/*
		 * Set the passed classes Highway's RoutePlannerPanel to this.
		 * Used to call methods of this class via the Highway.
		 */
		this.highway.setRoutePlannerPanel(this);
		
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		
		this.add(new MyLabel("First Encoutered: ", 20, 20, 100, 20));
		
		this.entry = new MyTextField(130, 20, 200, 20);
		this.entry.setEditable(false);
		this.add(this.entry);
		
		this.setEntry = new MyButton("Set First", 340, 20, 100, 20,this);
		this.add(this.setEntry);
		
		this.useEntryBranch = new JCheckBox("Use entry Branch?");
		this.useEntryBranch.setBounds(20,45,130,20);
		this.useEntryBranch.addActionListener(this);
		this.useEntryBranch.setVisible(true);
		this.add(this.useEntryBranch);
		
		this.add(new MyLabel("Last Encoutered: ", 20, 110, 100, 20));
		
		this.exit = new MyTextField(130, 110, 200, 20);
		this.exit.setEditable(false);
		this.add(this.exit);
		
		this.setExit = new MyButton("Set Last", 340, 110, 100, 20,this);
		this.add(this.setExit);
		
		this.useExitBranch = new JCheckBox("Use exit Branch?");
		this.useExitBranch.setBounds(20,135,130,20);
		this.useExitBranch.addActionListener(this);
		this.useExitBranch.setVisible(true);
		this.add(this.useExitBranch);
		
		this.saveRoute = new MyButton("Add to trip Plan", 160,450,150,20,this);
		this.add(this.saveRoute);
		
		this.obstacles = new MyTable();
		this.scroller = new JScrollPane();
		

		// Empty all fields and selected Obstacles
		this.nullify();
	}

	/**
	 * Parses the information provided by the user in the fields of this BasePanel into
	 * a Route.
	 * Then returns the finished Route.
	 * @return The finished Route.
	 * @throws Exception
	 */
	public Route toRoute() throws Exception{
		
		// Check whether a starting Obstacle was set.
		if(this.startObstacle == null){
			// If a staring Obstacle was not selected, inform the user of this mistake.
			Tools.error("Please select a valid Entry Obstacle.", this);
			throw new Exception();
		}
		
		// Check whether an ending Obstacle was set.
		if(this.endObstacle == null){
			// If an ending Obstacle was not selected, inform the user of this mistake.
			Tools.error("Please select a valid Exit Obstacle.", this);
			throw new Exception();
		}
		
		// Check whether the user wants to use an entry Branch, yet no Branch was selected.
		if(this.useEntryBranch.isSelected() == true && this.entryBranches.getSelectedIndex() == -1){
			// If an entry Branch is supposed to be used, yet none is selected, inform the user of this error.
			Tools.error("Please select a valid Entry Branch\nor do not use one.", this);
			throw new Exception();
		}

		// Check whether the user wants to use an exit Branch, yet no Branch was selected.
		if(this.useExitBranch.isSelected() == true && this.exitBranches.getSelectedIndex() == -1){
			// If an exit Branch is supposed to be used, yet none is selected, inform the user of this error.
			Tools.error("Please select a valid Exit Branch\nor do not use one.", this);
			throw new Exception();
		}
		
		boolean dir; // Prepare the direction boolean (true means up the Highway and false means down the Highway)
		
		// Check whether the staring Obstacle is also the ending Obstacle.
		if(this.startObstacle == this.endObstacle){
			// In such case let the user select whether the vehicle will be traveling up or down the highway.
			String selected = Tools.select(Tools.directions, "Up the Highway", "There is only one obstacle on the highway that you will encounter.\nPlease select the direction in which this route will be taken.");
			if(selected == null){
				// throw an Exception if the user pressed CANCEL instead of selecting a direction.
				throw new Exception();
			}
			// Set the selected direction as the direction boolean
			dir = (selected.equals("Up the Highway"));
		}else{
			/* If the starting and ending Obstacles are not the same one, the direction
			 * can be calculated by comparing their indexes.
			 */
			dir = (this.startObstacle.getIndex() < this.endObstacle.getIndex());
		}
		
		// Return a new Route, created of the gathered information from the fields on screen.
		return new Route(this.highway, this.startObstacle, this.endObstacle, this.highway.getBranch(this.entryBranches.getSelectedIndex()), this.highway.getBranch(this.exitBranches.getSelectedIndex()), this.useEntryBranch.isSelected(), this.useExitBranch.isSelected(), dir);
	}
	
	/**
	 * Rewrites the JTable of the Obstacles and the JComboBoxes of Branches
	 * with the current information.
	 */
	public void updatePanel() {
		
		// Set the check boxes about entry and exit Branches to unselected.
		this.useEntryBranch.setSelected(false);
		this.useExitBranch.setSelected(false);
		
		// If the Branch usage check boxes and Obstacle selection scroll pane exist, remove them from the BasePanel
		if(this.entryBranches != null){
			this.remove(this.entryBranches);
		}
		if(this.exitBranches != null){
			this.remove(this.exitBranches);
		}
		if(this.scroller != null){
			this.remove(this.scroller);
		}
		
		// Create a 2 Dimensional array for the JTable of Obstacles content. Amount of lines == Amount of Obstacles
		Object[][] data = new Object[this.highway.getObstacles()][4];
		
		// Create the titles of the columns of the JTable
		Object columns[] = {"Type", "Left", "Right", "Description"};
		
		// Iterate through all the Obstacles in the LinkedList of Obstacles for the Highway in the object variables.
		Obstacle temp = this.highway.getFirstObstacle();
		int loop = 0; // Counter of rows 
		while(temp != null){
			// Set up each row to contain the correctly formatted information about each Obstacle.
			data[loop][0] = "<html>" + temp.getType() + "<br><br><br><html>";
			data[loop][1] = "<html>" + temp.getLeft().getId() + "<br><br><br><html>";
			data[loop][2] = "<html>" + temp.getRight().getId() + "<br><br><br><html>";
			data[loop][3] = "<html>" + temp.getDescription() + "<br><br><br><html>";
			
			// Continue iteration
			temp = (Obstacle) temp.getNext();
			loop++;
		}
		// REPLACE THE OLD MYTABLE WITH A NEW UPDATED ONE
		
		// Make the JScrollPane and MyTable invisible
		this.scroller.setVisible(false);
		this.obstacles.setVisible(false);
		
		// Create the new MyTable with the new information
		this.obstacles = new MyTable(data, columns);
		
		// Set up the MyTable's formatting
		this.obstacles.getColumnModel().getColumn(0).setPreferredWidth(50);
		this.obstacles.getColumnModel().getColumn(1).setPreferredWidth(70);
		this.obstacles.getColumnModel().getColumn(2).setPreferredWidth(70);
		this.obstacles.getColumnModel().getColumn(3).setPreferredWidth(262);
		this.obstacles.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.obstacles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.obstacles.setRowHeight(60);
		
		// Add the MyTable into the new JScrollPane and place them onto the EditorPanel
		this.scroller = new JScrollPane(this.obstacles);
		this.scroller.setBounds(20,200,470,240);
		this.add(this.scroller);
		this.scroller.setVisible(true);
		this.obstacles.setVisible(true);
		
		
		// REPLACE THE JCOMBOBOXES WITH NEW UPDATED ONES
		
		/* Create an array for the new content of the JComboBoxes of length equal
		 * to the amount of Branches.
		 */
		Object items[] = new Object[this.highway.getBranches()];
		
		// Iterate through all the Branches in the LinkedList of Branches for the Highway in the object variables.
		Branch tempB = this.highway.getFirstBranch();
		for(int i = 0; tempB != null; i++){
			/*
			 * Fill each row with the Branch already correctly formated
			 * into text form.
			 */
			items[i] = tempB.getRestriction().getId() + " - " + tempB.getDescription();
			
			// Continue iteration.
			tempB = (Branch) tempB.getNext();
		}
		
		// Create a new entryBranches JComboBox with the current Branches.
		this.entryBranches = new JComboBox(items);
		// Format the new JComboBox.
		this.entryBranches.setBounds(50,70,400,20);
		this.entryBranches.setEnabled(false);
		this.add(this.entryBranches);
		// Add the new JComboBox to the BasePanel.
		this.entryBranches.setVisible(true);
		
		
		// Create a new exitBranch JComboBox with the current Branches.
		this.exitBranches = new JComboBox(items);
		// Format the new JComboBox.
		this.exitBranches.setBounds(50,160,400,20);
		this.exitBranches.setEnabled(false);
		this.add(this.exitBranches);
		// Add the new JComboBox to the BasePanel.
		this.exitBranches.setVisible(true);
		
		// Set all the texts int fields and entry/exit Obstacles to empty.
		this.nullify();
		
		// Refresh the screen.
		this.validate();
	}

	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		
		/* 
		 * If the JCheckBox for using an entry Branch was pressed, set the JComboBox corresponding
		 * to the JCkeckBox as enabled or disabled accordingly.
		 */
		if(e.getSource() == this.useEntryBranch){
			this.entryBranches.setEnabled(this.useEntryBranch.isSelected());
		}
		
		/* 
		 * If the JCheckBox for using an exit Branch was pressed, set the JComboBox corresponding
		 * to the JCkeckBox as enabled or disabled accordingly.
		 */
		if(e.getSource() == this.useExitBranch){
			this.exitBranches.setEnabled(this.useExitBranch.isSelected());
		}
		
		// Button to set the start Obstacle as the entry Obstacle was pressed
		if(e.getSource() == this.setEntry){
			
			/* set the start Obstacle as the Obstacle from the LinkedList of Obstacles for
			 * the object variable Highway, getting its index by using the index of the selected row
			 * in the MyTable of Obstacles. 
			 */
			this.startObstacle = this.highway.getObstacle(this.obstacles.getSelectedRow());
			
			if(this.startObstacle == null){
				// If the selected Obstacle is null (nothing was selected), inform the user of this error.
				Tools.error("Invalid obstacle selected",this);
			}else{
				// If a valid Obstacle was selected, display its ID information if the corresponding MyTextArea
				this.entry.setText(this.startObstacle.getType() + " - " + this.startObstacle.getLeft().getId() + " - " + this.startObstacle.getRight().getId());
			}
		}
		
		
		// Button to set the selected Obstacle as the end Obstacle was pressed
		if(e.getSource() == this.setExit){
			
			/* set the end Obstacle as the Obstacle from the LinkedList of Obstacles for
			 * the object variable Highway, getting its index by using the index of the selected row
			 * in the MyTable of Obstacles. 
			 */
			this.endObstacle = this.highway.getObstacle(this.obstacles.getSelectedRow());
			if(this.endObstacle == null){
				// If the selected Obstacle is null (nothing was selected), inform the user of this error.
				Tools.error("Invalid obstacle selected",this);
			}else{
				// If a valid Obstacle was selected, display its ID information if the corresponding MyTextArea
				this.exit.setText(this.endObstacle.getType() + " - " + this.endObstacle.getLeft().getId() + " - " + this.endObstacle.getRight().getId());
			}
		}
		
		// Button to save the Route was pressed.
		if(e.getSource() == this.saveRoute){
			try {
				/*
				 * Call the parent's addRoute method and pass it a Route formed out of the information
				 * the user provided in this BasePanel in the parameters.
				 */
				parent.addRoute(this.toRoute());
			} catch (Exception ex) {
				// If the route could not be created (invalid information was entered), do nothing.
			}
		}
	}
	
	/**
	 * Erase all potentially selected information from the object variables.
	 */
	public void nullify(){
		// Set text fields to empty.
		this.entry.setText("");
		this.exit.setText("");
		
		// Set start and end Obstacles to null.
		this.startObstacle = null;
		this.endObstacle = null;
	}
}
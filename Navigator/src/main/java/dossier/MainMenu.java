package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Handles the GUI of the main menu. Extends BasePanel.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 * 
 */

public class MainMenu extends BasePanel implements ActionListener{
	private static final long serialVersionUID = -7356489042166939250L;

	/* Pointer to the Guru class that created this class. (Used to call
	 * methods of Guru).
	 */
	private Guru guru;

	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton planRoute;
	private MyButton editMap;
	private MyButton management;
	private MyButton aboutProgram;
	
	/**
	 * Constructor saves the pointer passed in the parameters to the
	 * object variable and creates the GUI.
	 * 
	 * @param guru The Guru class that created this class.
	 */
	public MainMenu(Guru guru){
		super(); // Call to the constructor of the superclass.
		
		// Save the pointers passed in the parameters as the object variables.
		this.guru = guru;
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.planRoute = new MyButton("Plan Route",300,150,200,100,this);
		this.editMap = new MyButton("Edit Map",300,260,200,50,this);
		this.management = new MyButton("Management",300,320,200,50,this);
		this.aboutProgram = new MyButton("About Program",300,430,200,50,this);
		
		this.add(this.planRoute);
		this.add(this.editMap);
		this.add(this.management);
		this.add(this.aboutProgram);
	}

	/**
	 * The action listener
	 */
	public void actionPerformed(ActionEvent e) {
		
		/*
		 * SINGLE PURPOSE
		 * 
		 * Use guru.setScreen() to switch to the screen corresponding to the pressed button.
		 */
		
		if(e.getSource() == this.planRoute){
			this.guru.setScreen(Screens.TRIP_SETUP);
		}
		
		if(e.getSource() == this.editMap){
			this.guru.setScreen(Screens.EDITOR);
		}
		
		if(e.getSource() == this.management){
			this.guru.setScreen(Screens.MANAGEMENT);
		}
		
		if(e.getSource() == this.aboutProgram){
			this.guru.setScreen(Screens.ABOUT_PROGRAM);
		}
	}
}

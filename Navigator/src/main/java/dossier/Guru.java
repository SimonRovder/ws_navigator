package dossier;

/**
 * Takes care of the launching system and contains the algorithm that assesses the pass of vehicles over bridges.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class Guru extends MyFrame
{
	// The main panels. These will be set as the contentPanes when the user switches the screens.
    
	private static final long serialVersionUID = -7589416400666234047L;
	private MainMenu mainMenu;
    private Management management;
    private TripSetup tripSetup;
    public RoutePlanner routePlanner;
    public Editor editor;
    private OutputScreen outputScreen;
    private AboutProgram aboutProgram;
    
    // The trip. This is the list of all routes a vehicle will take on its trip.
    private Trip trip;
    
    // File management class. The only class that reads and writes into files.
    public FileManager fileManager;
    
    // The Data structure for the entire system of highways
    public HighwaySystem highwaySystem;
    
    /**
     * The main constructor of the whole program.
     * Ensures the launch of the program
     */
    public Guru(){
    	// Call super constructor and create the window
        super("Navigator");
        this.setSize(800,600);
        // Place the window into the center of the screen.
        this.centerPosition();
        this.setResizable(false);
        
        // Create the empty trip
        this.trip = new Trip();
        
        // Create the main screens (pass self and the trip to some of them)
        this.mainMenu = new MainMenu(this);
        this.management = new Management(this);
        this.tripSetup = new TripSetup(this, this.trip);
        this.routePlanner = new RoutePlanner(this, this.trip);
        this.editor = new Editor(this);
        this.outputScreen = new OutputScreen(this);
        this.aboutProgram = new AboutProgram(this);
        
        // Create the file manager and hence prepare for initialization
        this.fileManager = new FileManager();
        
        try{
			this.highwaySystem = this.fileManager.initialize();		// Try to load the current Direct Access Files
			this.editor.createPanels(); 		// Try to create the panels of the TabbedPane in the editor
	        this.routePlanner.createPanels(); 	// Try to create the panels of the TabbedPane in the route planner
        }catch(Exception ex){
        	// Initialization must have failed (for whatever reason). Inform the user of this error.
        	Tools.error("LAUNCH FAILED!\n\nThe launching of the program failed. This could be the result of\n" +
        			"damaged initialization files. An emergency re-initialization\nwill now take place and you will" +
        			"be redirected to the\nmain menu screen.\n" + ex.getMessage() + 
        			"\nThe program will continue with a factory reset.", this);
        			// Kindly inform the user that his data is gone and the program will reset.
        	// Initiate a factory reset.
        	this.factoryReset();
    	}
        
        this.setScreen(Screens.MAIN_MENU);       			// Set the initial Screen
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);	// Set the close operation
        this.setVisible(true);							// Display the window
    
    }
    /**
     * Performs a factory reset - Loads an empty backup file the content of which is hard-wired
     * into the program.
     */
    public void factoryReset() {
    	try {
    		/* Create an empty backup file that has no highways in it, with all the necessary information
    		 * required to load it.
    		 */
        	Append reseter = new Append();
			reseter.open("FactoryResetBackupFile", false);
        	reseter.write("300");
        	reseter.write("0");
        	reseter.close();
        	
        	// Load the empty backup file that was just created (ensured safe loading)
        	this.highwaySystem = this.fileManager.loadBackup("FactoryResetBackupFile");
        	
        	// And create the tabs in the Editor and RoutePlanner
			this.editor.createPanels();
	        this.routePlanner.createPanels();
	        
		}catch (Exception ex2){
			/* If this occurs, something terrible must have happened. Really, if this did not work, the program
			 * is really gone.
			 */
			Tools.error("An ultimately fatal error occured. Please\ndo not run this program again and\n" +
					"contact the main developer at\nsimon.rovder@gmail.com\nfor further assistance.", this);
			System.exit(ERROR);
		}
	}

	/**
     * Change the Screen (set the content pane as the corresponding panel).
     * @param id -> An enum value of class Screens.
     */
    
    public void setScreen(Screens id){
    	switch (id){
			case MAIN_MENU:
				this.setContentPane(this.mainMenu);
				break;
			case EDITOR:
				this.setContentPane(this.editor);
				break;
			case MANAGEMENT:
				this.setContentPane(this.management);
				break;
			case ROUTE_PLANNER:
				this.setContentPane(this.routePlanner);
				break;
			case TRIP_SETUP:
				this.setContentPane(this.tripSetup);
				break;
			case OUTPUT_SCREEN:
				this.setContentPane(this.outputScreen);
				break;
			case ABOUT_PROGRAM:
				this.setContentPane(this.aboutProgram);
    	}
    	this.validate(); // Validate the window to show the change
    }
    
    /**
     * Initialization
     * @param args
     */
    public static void main(String args[]){
    	new Guru();
    }

    /**
     * The algorithm that processes and evaluates the actual trip of the vehicle through the
     * routes specified in the trip
     */
	public void process() {
		// Create a new empty Report that will contain the results of this assessment.
		Report report = new Report();
		
		
		Route route = (Route) this.trip.getFirst();	// Get the first Route in the Trip.
		Obstacle obstacle;							// Prepare an Obstacle.
		
		// Iterate through all the Routes in the Trip, assessing them during the process.
		while(route != null){
			
			// Check whether the vehicle uses an entry Branch during this Route.
			if(route.isUseEntryBranch()){
				// If so, assess the Branch.
				this.assess(report, route.getEntry().getRestriction());
			}
			
			/*
			 * Check whether the vehicle is moving up or down the highway.
			 * Assess the appropriate side of the Obstacle (left or right)
			 * for the given direction.
			 */
			if(route.getDirection()){ // The vehicle is going up the Highway.
				obstacle = route.getStart();	// Get the first Obstacle on the Route
				while(obstacle != null){		// Iterate through all the obstacles, assessing them one by one.
					this.assess(report, obstacle.getRight()); // Assess the appropriate side.
					
					// Check whether the last Obstacle was reached.
					if(obstacle == route.getEnd()){
						// Break if the last Obstacle was reached.
						break;
					}
					
					// Continue iteration with the next Obstacle on the Route. (appropriate direction)
					obstacle = (Obstacle) obstacle.getNext();
				}
			}else{ // The vehicle is going up the Highway.
				obstacle = route.getStart();	// Get the first Obstacle on the Route
				while(obstacle != null){		// Iterate through all the obstacles, assessing them one by one.
					this.assess(report, obstacle.getLeft()); // Assess the appropriate side.
					
					// Check whether the last Obstacle was reached.
					if(obstacle == route.getEnd()){
						// Break if the last Obstacle was reached.
						break;
					}
					
					// Continue iteration with the next Obstacle on the Route. (appropriate direction)
					obstacle = (Obstacle) obstacle.getPrevious();
				}
			}
			
			// Check whether the vehicle uses an exit Branch during this Route.
			if(route.isUseExitBranch()){
				// If so, assess the Branch.
				this.assess(report, route.getExit().getRestriction());
			}
			
			// Continue iteration with the next Route in the Trip.
			route = (Route) route.getNext();
		}
		
		// Once all Routes have been assessed, load the resulting Report into the OutputScreen.
		this.outputScreen.loadScreen(report,this.trip);
		
		// Switch the view to the OutputScreen to display the results.
		this.setScreen(Screens.OUTPUT_SCREEN);
	}
	
	/**
	 * Evaluates the vehicles pass through the given restriction.
	 * @param report The Report into which the result should be written
	 * @param restriction The Restriction that is to be assessed.
	 */
	
	private void assess(Report report, Restriction restriction){
		// Prepare the assessment Strings for weight and height.
		String statusHeight = "";
		String statusWeight = "";
		
		// Check to see whether the Restriction poses a height limit.
		if(restriction.isUseHeights()){
			// If so, assess the height limit and store the result in the height String
			statusHeight = this.assessHeight(restriction);
		}
		
		// Check to see whether the Restriction poses a weight limits.
		if(restriction.isUseWeights()){
			// If so, assess the height limits and store the result in the weight String
			statusWeight = this.assessWeights(restriction);
		}
		
		// Read the ID of the Restriction that is to be written in the Report.
		String id = restriction.getId();
		if(id.equals("")){
			// If there is no ID, assign it the text "Unknown ID" instead.
			id = "Unknown ID";
		}
		
		if(statusWeight.equals("CRASH") || statusHeight.equals("CRASH")){
			// If either of the two result Strings say "CRASH", report the Restriction as crashed.
			report.crash = report.crash + id + ", ";
			return;
		}
		if(statusWeight.equals("UNKNOWN") || statusHeight.equals("UNKNOWN")){
			// If either of the two result Strings say "UNKNOWN", report the Restriction as unknown.
			report.unknown = report.unknown + id + ", ";
			return;
		}
		if(statusWeight.equals("EXCLUSIVE")){
			// If weight limit result String say "EXCLUSIVE", report the Restriction as exclusive.
			report.exclusive = report.exclusive + id + ", ";
			return;
		}
		if(statusWeight.equals("EXTRAORDINARY")){
			// If weight limit result String say "EXTRAORDINARY", report the Restriction as extraordinary.
			report.extraordinary = report.extraordinary + id + ", ";
			return;
		}
		/*
		 * If neither of the previous conditions have been met, the Restriction
		 * either does not pose any limitations that could not be passed normally,
		 * or it does not pose any restrictions at all. In any case, the
		 * Restriction can be reported as normal. So report it as such.
		 */
		report.normal = report.normal + id + ", ";
	}
	
	/**
	 * Assesses the height of the vehicle against the provided Restriction.
	 * @param restriction The Restriction that is to be assessed.
	 * @return "NORMAL" or "CRASH", depending on result
	 */
	private String assessHeight(Restriction restriction){
		if(restriction.getHeight() != -1){ // Check whether the height limit is known.
			/*
			 * If it is known, compare it to the height of the vehicle and return the
			 * corresponding String.
			 * NORMAL if the height of the vehicle is smaller than the height limit.
			 * CRASH if the height of the vehicle exceeds the weight limit.
			 */
			if(restriction.getHeight() > this.trip.getHeight()){
				return "NORMAL";
			}else{
				return "CRASH";
			}
		}else{
			// If the height limit is unknown, return UNKNOWN.
			return "UNKNOWN";
		}
	}
	
	/**
	 * Assesses the weight of the vehicle against the given obstacle.
	 * @param restriction The Restriction that is to be assessed.
	 * @return "NORMAL", "EXCLUSIVE", "EXTRAORDINARY", "UNKNOWN" or "CRASH", depending on the result
	 */
	private String assessWeights(Restriction restriction){
		double max = 0;		// Create the double that will monitor the highest weight measured so far.
		double next = 0;	// Create the double that will store the next assessed weight combination.
		int start = 0;		// The starting point of the axle separation array.
		int end = 0;		// The ending point of the axle separation array.
		
		if(restriction.getSeparation() == -1){
			/* If the Restriction is of an unknown length, the maximum weight
			 * that can be located on the restriction is the total weight of the vehicle.
			 */
			max = this.trip.getTotalWeight();
		}else{
			// If the length is known, the highest possible combination must be found
			while(end < this.trip.getSeparation().length){
				// While there are still more axles, continue looking for the highest combination.
				
				/* Sum up all the distances between the starting point of the axle separation array
				 * to the ending point of the axle separation array.
				 */
				if(this.sum(this.trip.getSeparation(), start, end) <= restriction.getSeparation()){
					/*
					 * If this length is still smaller than the length of the Restriction, sum together
					 * all the weights that are within that axle range. 
					 */
					next = this.sum(this.trip.getWeight(), start, end + 1);
					// Compare the resultant weight with the greatest weight yet.
					if(next > max){
						// Replace the old maximum weight with the new weight if it is greater.
						max = next;
					}
					/* Move the ending point of the axle separation array up by one to increase
					 * the assessed distance.
					 */
					end++;
				}else{
					/*
					 * If the distance the starting and ending points of the axle separation array
					 * enclose a greater length than is the length of the Restriction, move the
					 * starting point of the axle separation array up by one to shorten the range.
					 */
					start++;
				}
			}
		}
		/* Start the weight comparison recursive algorithm by providing the Restriction,
		 * maximum weight and the step of the recursion (current weight limit).
		 */
		return this.compareWeights(restriction, max, 0);
	}
	
	/**
	 * Assesses the maximum weight against the provided obstavle's level. This algorithm is recursive
	 * and only stops once reaching a certain level of depth (the amount of weight limitations,
	 * which is 3)
	 * 
	 * @param restriction Assessed Restriction
	 * @param max Maximum weight ever reached on the Restriciton
	 * @param level The assessed weight level :
	 * Normal - 0
	 * Exclusive - 1
	 * Extraordinary - 2
	 * @return "NORMAL", "EXCLUSIVE", "EXTRAORDINARY", "UNKNOWN" or "CRASH", depending on the result
	 */
/*
 * MASTERY FACTOR 4 - Recursion.
 */
	private String compareWeights(Restriction restriction, double max, int level){
		if(level < 3){ // Check whether the current level is still within the three possible weight levels.
			
			/*
			 * Check whether the current weight level limit is known. If it is known,
			 * assess it (within the IF statement). If it is not known, skip the
			 * assessment procedure.
			 */
			if(restriction.getWeight(level) != -1){
				
				// Check whether the current weight limit exceeds the maximum weight of the vehicle.
				if(restriction.getWeight(level) > max){
					/* If the current weight limit exceeds the maximum weight of the vehicle, return
					 * the String corresponding to the current weight limit.
					 */
					switch(level){
						case 0 :
							return "NORMAL";
						case 1 :
							return "EXCLUSIVE";
						case 2 :
							return "EXTRAORDINARY";
					}
				}else{
					/*
					 * If this point is reached, the current weight limit is smaller than the maximum
					 * weight that will be on the Restriction. If the level is smaller than the maximum
					 * possible level, do nothing. If the current level is already the maximum
					 * possible level (and it is known that the maximum weight exceeds it), the
					 * Restriction should be assessed as crashed. So "CRASH" is returned. 
					 */
					if(level == 2){
						return "CRASH";
					}
				}
			}
			
			/*
			 * Whenever the program reaches this point, it means the previous weight levels were either
			 * unknown, or insufficient. Thus the level is increased and the recursion continues by calling
			 * self.
			 */
			level++;
			return this.compareWeights(restriction, max, level);
		}else{
			/*
			 * If the weight level exceeded 2 (beyond EXTRAORDINARY), report the passing of Restriction
			 * as UNKNOWN. The assessment would have been complete by this point, had any appropriate
			 * weight level been known.
			 */
			return "UNKNOWN";
		}
	}
	
	/**
	 * Sums up all the double type values in the array from the starting location to the ending location.
	 * @param array The array of Doubles
	 * @param start The index of the starting point in the array.
	 * @param end The index of the ending point in the array.
	 * @return The sum.
	 */
	private double sum(double[] array, int start, int end){
		double sum = 0; 				// Create the double in which the values will be summed up.
		while(start <= end){ 			// Iterate while the starting point does not reach the ending point.
			sum = sum + array[start];	// Add the current record in the array to the sum.
			start++;					// Increase the starting point (go to next array record).
		}
		return sum; 					// Return the summed up value.
	}

	
	/**
	 * Ensures the exiting of the trip setup and route assessment screens. It erases the data provided in those
	 * sections, as the user will have the ability to edit them once in the main menu. 
	 */
	public void exitTripSetupMode() {
		// Nullify the Trip (erase set Routes).
		this.trip.nullify();
		// Refresh the JList of Routes.
		this.routePlanner.refreshRoutes();
		// Nullify the RoutePlanner.
		this.routePlanner.nullify();
		// Nullify the Trip information (about the vehicle).
		this.tripSetup.nullify();
		// Set the screen to the main menu.
		this.setScreen(Screens.MAIN_MENU);
	}
}
package dossier;

/**
 * Class containing the information about and Branches and Obstacles of a highway.
 * 
 * Extends the Node, as it will be a part of a LinkedList.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 * 
 */

/*
 * MASTERY FACTOR 7 - Inheritance.  
 */
public class Highway extends Node{
	
	// The name of the Highway.
	private String name;
	
	// The LinkedList of Obstacles located on this Highway.
	private LinkedList obstacles;
	
	// The LinkedList of Obstacles located on this Highway.
	private LinkedList branches;
	
	/*
	 *  The EditorPanel corresponding to this Highway.
	 *  (Pointer used to update the panel when the Highway is changed in any way.
	 */
	private EditorPanel editorPanel;
	
	/*
	 *  The RoutePlannerPanel corresponding to this Highway.
	 *  (Pointer used to update the panel when the Highway is changed in any way.
	 */
	private RoutePlannerPanel routePlannerPanel;

	/**
	 * Constructor creates the Highway with the name passed in the parameters.
	 * @param name The name of the Highway.
	 */
	public Highway(String name){
		super(); // Call to the contructor of the superclass.
		

		// Save the name of the Highway as the object variable.
		this.name = name;
		
		// Create the LinkedLists of Obstacles and Branches.
		this.branches = new LinkedList();
		this.obstacles = new LinkedList();
		
		//Nullify all the content of this Highway.
		this.nullify();
	}
	
	/**
	 * Erases all Obstacles and Node connections of this Highway.
	 * (keeps the name)
	 */
	private void nullify(){
		// Nullify the LinkedLists of the Obstacles and Branches.
		this.obstacles.nullify();
		this.branches.nullify();
		
		// Erase the potential onnections to other Nodes.
		this.next = null;
		this.previous = null;
	}
	
	/**
	 * Adds the provided Obstacle to the end of the LinkedList of Obstacles.
	 * @param obstacle The new Obstacle.
	 */
	public void addToEnd(Obstacle obstacle){
		this.obstacles.addToEnd(obstacle);
	}
	
	/**
	 * Adds the provided Branch to the end of the LinkedList of Branches.
	 * @param branch The new Branch.
	 */
	public void addToEnd(Branch branch){
		this.branches.addToEnd(branch);
	}
	
	/**
	 * Returns the Obstacle on the provided position in the LinkedList of Obstacles.
	 * @param index The position of the Obstacle in the LinkedList.
	 * @return The Obstacle.
	 */
	public Obstacle getObstacle(int index){
		return (Obstacle) this.obstacles.getNode(index);
	}
	
	/**
	 * Returns the Branch on the provided position in the LinkedList of Branches.
	 * @param index The position of the Branch in the LinkedList.
	 * @return The Branch.
	 */
	public Branch getBranch(int num){
		return (Branch) this.branches.getNode(num);
	}
	
	/**
	 * Deletes the provided Branch from the LinkedList of Branches.
	 * @param branch The Branch that is to be deleted.
	 * @throws Exception
	 */
	public void delete(Branch branch) throws Exception{
		Branch looper = branch; // Prepare a Pointer to the provided Branch for iteration.
		while(looper != null){
			/* 
			 * Gradually decrease the indexes of the Branches following the one that is to be deleted, to
			 * account for the change in the list.
			 */
			looper.setIndex(looper.getIndex() - 1);
			
			// Continue iteration with the next Branch.
			looper = (Branch) looper.getNext();
		}
		this.branches.delete(branch); // Delete the Branch from the LinkedList.
	}
	
	/**
	 * Deletes the provided Obstacle from the LinkedList of Branches.
	 * @param obstacle The Obstacle that is to be deleted.
	 * @throws Exception
	 */
	public void delete(Obstacle obstacle) throws Exception{
		Obstacle looper = obstacle; // Prepare a Pointer to the provided Obstacle for iteration.
		while(looper != null){
			/* 
			 * Gradually decrease the indexes of the Obstacles following the one that is to be deleted, to
			 * account for the change in the list.
			 */
			looper.setIndex(looper.getIndex() - 1);

			// Continue iteration with the next Obstacle.
			looper = (Obstacle) looper.getNext();
		}
		this.obstacles.delete(obstacle); // Delete the Obstacle from the LinkedList.
	}

	/**
	 * Adds the provided Obstacle to the provided position in the LinkedList of Obstacles.
	 * @param obstacle The Obstacle that is to be added.
	 * @param index The index to which the provided Obstacle.
	 */
	public void addToPos(Obstacle obstacle, long index){
		this.obstacles.addToPos(obstacle, index); // Add the Obstacle to the LinkedList of Obstacles.
		obstacle = (Obstacle) obstacle.getNext(); // Jump to the Next Obstacle.
		while(obstacle != null){
			/*
			 * Iterate through all the Obstacles in the List, gradually changing increasing all their
			 * indexes by one, to account for the added Obstacle.
			 */
			obstacle.setIndex(obstacle.getIndex() + 1);
			// Continue iteration with the next Obstacle.
			obstacle = (Obstacle) obstacle.getNext();
		}
	}
	
	/**
	 * Adds the provided Branch to the provided position in the LinkedList of Branches.
	 * @param branch The Branch that is to be added.
	 * @param index The index to which the provided Branch.
	 */
	public void addToPos(Branch branch, long index){
		this.branches.addToPos(branch, index); // Add the Branch to the LinkedList of Branches.
		branch = (Branch) branch.getNext(); // Jump to the Next Branch.
		while(branch != null){
			/*
			 * Iterate through all the Branches in the List, gradually changing increasing all their
			 * indexes by one, to account for the added Branch.
			 */
			branch.setIndex(branch.getIndex() + 1);
			// Continue iteration with the next Branch.
			branch = (Branch) branch.getNext();
		}
	}
	
	/**
	 * Getter for the first Obstacle in the LinkedList of Obstacles.
	 * @return The first Obstacle in the LinkedList of Obstacles.
	 */
	public Obstacle getFirstObstacle() {
		return (Obstacle) this.obstacles.getFirst();
	}

	/**
	 * Getter for the first Branch in the LinkedList of Branches.
	 * @return The first Branch in the LinkedList of Branches.
	 */
	public Branch getFirstBranch() {
		return (Branch) this.branches.getFirst();
	}

	/**
	 * Getter for the name of this Highway.
	 * @return The name of this Highway.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Getter for the amount of Obstacles on this Highway.
	 * @return The amount of Obstacles on this Highway.
	 */
	public int getObstacles() {
		return this.obstacles.getAmount();
	}
	
	/**
	 * Getter for the amount of Branches on this Highway.
	 * @return The amount of Branches on this Highway.
	 */
	public int getBranches() {
		return this.branches.getAmount();
	}
	
	/**
	 * Getter fot the EditorPanel corresponding to this Highway.
	 * @return The EditorPanel corresponding to this Highway.
	 */
	public EditorPanel getEditorPanel() {
		return editorPanel;
	}

	/**
	 * Setter for the EditorPanel corresponding to this Highway.
	 * @param editorPanel The new EditorPanel corresponding to this Highway/
	 */
	public void setEditorPanel(EditorPanel editorPanel) {
		this.editorPanel = editorPanel;
	}
	
	/**
	 * The Setter for the new RoutePlannerPanel corresponding to this Highway.
	 * @param routePlannerPanel The new RoutePlannerPanel corresponding to this Highway.
	 */
	public void setRoutePlannerPanel(RoutePlannerPanel routePlannerPanel) {
		this.routePlannerPanel = routePlannerPanel;
	}
	
	/**
	 * The getter for the RoutePlannerPanel corresponding to this Highway.
	 * @return The RoutePlannerPanel corresponding to this Highway.
	 */
	public RoutePlannerPanel getRoutePlannerPanel() {
		return this.routePlannerPanel;
	}
}

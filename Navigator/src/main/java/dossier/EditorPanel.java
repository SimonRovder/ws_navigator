package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 * The EditorPanel class creates the GUI for the editing of Obstacles and Highways. It extends the BasePanel.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class EditorPanel extends JPanel implements ActionListener{
	private static final long serialVersionUID = 7226173943668242551L;
	// The Components used by the GUI
	private MyButton switcher;
	private MyButton addBefore;
	private MyButton addAfter;
	private MyButton edit;
	private MyButton delete;
	private MyButton addFirst;
	private MyButton moveUp;
	private MyButton moveDown;
	private MyLabel title;
	private JScrollPane obstacleScroller;
	private JScrollPane branchScroller;
	private MyTable branchTable;
	private MyTable obstacleTable;
	
	private Highway highway; // Pointer to the Highway this EditorPanel corresponds to
	private Editor parent; // Pointer to the Editor itself (for triggering functions)
	
	private boolean mode; // Differentiate between editing Branches and Obstacles.
	
	/**
	 * Constructor
	 * @param highway The Highway this EditorPanel corresponds to
	 * @param editor The Editor that created this EditorPanel
	 */
	public EditorPanel(Highway highway, Editor editor){
		
		super(); // Call to the counstructor of the super class
		this.setLayout(null);
		
		// Setting the object variables to the parameters 
		this.highway = highway;
		this.highway.setEditorPanel(this);
		this.parent = editor;
		this.mode = true;
		
		// CREATING THE GUI
		
		this.title = new MyLabel("<html><h1>OBSTACLES</h1></html>",550,20,170,50);
		this.add(this.title);
		
		this.switcher = new MyButton("Go to Branches",550,90,170,40,this);
		this.add(this.switcher);
		
		this.moveUp = new MyButton("UP", 550,170,80,30,this);
		this.add(this.moveUp);
		
		this.moveDown = new MyButton("DOWN", 640,170,80,30,this);
		this.add(this.moveDown);
		
		this.addBefore = new MyButton("Add before",550,210,170,30,this);
		this.add(this.addBefore);
		
		this.addAfter = new MyButton("Add after",550,250,170,30,this);
		this.add(this.addAfter);
		
		this.edit = new MyButton("Edit",550,290,170,30,this);
		this.add(this.edit);
		
		this.delete = new MyButton("Delete",550,330,170,30,this);
		this.add(this.delete);
		
		this.addFirst = new MyButton("Add First",550,370,170,30,this);
		this.add(this.addFirst);

		this.obstacleScroller = new JScrollPane();
		this.obstacleTable = new MyTable();
		this.branchScroller = new JScrollPane();
		this.branchTable = new MyTable();
	}
	
	
	/**
	 * Updates the JTables of the Branches and the Obstacles with the current information.
	 */
/*
 * MASTERY FACTOR 19 - Arrays of two or more dimensions.
 */
	public void updatePanels(){
		
		// Create a 2 Dimensional array for the MyTable of Obstacles content. Amount of lines == Amount of Obstacles
		Object[][] data = new Object[this.highway.getObstacles()][3];
		
		// Create the titles of the columns of the MyTable
		Object columns[] = {"Description", "Left Lane", "Right Lane"};
		
		// Iterate through all the Obstacles in the LinkedList of Obstacles for the Highway in the object variables.
		Obstacle temp = this.highway.getFirstObstacle();
		int loop = 0; // Counter of rows 
		while(temp != null){
			// Set up each row to contain the correctly formatted information about each Obstacle.
			data[loop][0] = "<html>" + temp.getType() + "<br>" + temp.getDescription() + "<br>Kilometer: " + Tools.isUnknown(temp.getKm()) + "<br>Index: " + temp.getIndex() + "<br><br><br><html>";
			data[loop][1] = temp.getLeft().toHTMLString();
			data[loop][2] = temp.getRight().toHTMLString();
			
			// Continue iteration
			temp = (Obstacle) temp.getNext();
			loop++;
		}
		
		
		// REPLACE THE OLD MYTABLE WITH A NEW UPDATED ONE
		
		// Make the JScrollPane and MyTable invisible
		this.obstacleScroller.setVisible(false);
		this.obstacleTable.setVisible(false);
		
		// Create the new MyTable with the new information
		this.obstacleTable = new MyTable(data, columns);
		
		// Set up the MyTable's formatting
		this.obstacleTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.obstacleTable.getColumnModel().getColumn(0).setPreferredWidth(260);
		this.obstacleTable.getColumnModel().getColumn(1).setPreferredWidth(120);
		this.obstacleTable.getColumnModel().getColumn(2).setPreferredWidth(120);
		this.obstacleTable.setRowHeight(100);
		this.obstacleTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Add the MyTable into the new JScrollPane and place them onto the EditorPanel
		this.obstacleScroller = new JScrollPane(this.obstacleTable);
		this.obstacleScroller.setBounds(20,20,520,400);
		this.add(this.obstacleScroller);
		
		
		// DO THE SAME FOR THE BRANCHES
		
		// Create a 2 Dimensional array for the MyTable of Obstacles content. Amount of lines == Amount of Branches
		data = new Object[this.highway.getBranches()][2];
		
		// Create the titles of the columns of the MyTable
		Object columns2[] = {"Description", "Limitations"};
		
		// Iterate through all the Branches in the LinkedList of Branches for the Highway in the object variables.
		Branch temp2 = this.highway.getFirstBranch();
		loop = 0; // Counter of rows is reset to zero.
		while(temp2 != null){
			// Set up each row to contain the correctly formatted information about each Branch.
			data[loop][0] = "<html>" + temp2.getDescription() + "<br>Kilometer: " + Tools.isUnknown(temp2.getKm()) + "<br>Index: " + temp2.getIndex() + "<br><br><br><br><html>";
			data[loop][1] = temp2.getRestriction().toHTMLString();
			
			// Continue iteration
			temp2 = (Branch) temp2.getNext();
			loop++;
		}
		
		// Replace the old MyTable with a new updated one.
		
		// Make the JScrollPane and MyTable invisible
		this.branchScroller.setVisible(false);
		this.branchTable.setVisible(false);
		
		// Create the new MyTable with the new information
		this.branchTable = new MyTable(data, columns2);
		
		// Set up the MyTable's formatting
		this.branchTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.branchTable.getColumnModel().getColumn(0).setPreferredWidth(300);
		this.branchTable.getColumnModel().getColumn(1).setPreferredWidth(200);
		this.branchTable.setRowHeight(100);
		this.branchTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// Add the MyTable into the new JScrollPane and place them onto the EditorPanel
		this.branchScroller = new JScrollPane(this.branchTable);
		this.branchScroller.setBounds(20,20,520,400);
		this.add(this.branchScroller);
		
		// Refresh the screen to show the changes.
		this.validateScreen();
	}
	
	/**
	 * Ensures correct Components are visible on the screen (differentiate between Obstacle editing and Branch editing modes)
	 */
	private void validateScreen(){
		if(this.mode){ // The user is editing Obstacles
			
			// Make the JTable of Branches invisible
			this.branchScroller.setVisible(false);
			
			// Make the JTable of Obstacles visible
			this.obstacleScroller.setVisible(true);
			
			// Set the texts displayed to match the mode
			this.switcher.setText("Go to Branches");
			this.title.setText("<html><h1>OBSTACLES</h1></html>");
			
		}else{ // The user is editing Branches
			
			// Make the JTable of Branches invisible
			this.branchScroller.setVisible(true);
			
			// Make the JTable of Obstacles visible
			this.obstacleScroller.setVisible(false);
			
			// Set the texts displayed to match the mode
			this.switcher.setText("Go to Obstacles");
			this.title.setText("<html><h1>BRANCHES</h1></html>");
		}
		this.validateButtons(); // Make sure the buttons are correctly enabled for the amounts of records.
	}
	
	/**
	 * Ensures buttons are correctly enabled or disabled, depending on the amount of Obstacles or Branches
	 * on the current Highway and which of them is showing. 
	 */
	private void validateButtons(){
		if(this.mode){ // The JTable of Obstacles is visible and being edited
			if(this.obstacleTable.getRowCount() == 0){
				/* If there are no Obstacle on the Highway, it cannot be determined after or before which Obstacle
				 * do you which to add a new one. Thus the buttons for adding before and adding after are disabled.
				 * The user can only add a first Obstacle, so the button for this is enabled. Similar cases apply to
				 * the other buttons too.
				 */
				this.addFirst.setEnabled(true);
				this.addBefore.setEnabled(false);
				this.addAfter.setEnabled(false);
				this.moveDown.setEnabled(false);
				this.moveUp.setEnabled(false);
				this.edit.setEnabled(false);
				this.delete.setEnabled(false);
			}else{
				/* If there are Obstacles on the Highway, the user is only enabled to add new Obstacles after or before
				 * an existing obstacle. So the buttons to add before and after are enabled, while the button to add a
				 * first Obstacle is disabled. Similar cases apply to the other buttons too.
				 */
				this.addFirst.setEnabled(false);
				this.addBefore.setEnabled(true);
				this.addAfter.setEnabled(true);
				this.moveDown.setEnabled(true);
				this.moveUp.setEnabled(true);
				this.edit.setEnabled(true);
				this.delete.setEnabled(true);
			}
		}else{
			if(this.branchTable.getRowCount() == 0){
				/* If there are no Branches on the Highway, it cannot be determined after or before which Branch
				 * do you which to add a new one. Thus the buttons for adding before and adding after are disabled.
				 * The user can only add a first Branch, so the button for this is enabled. Similar cases apply to
				 * the other buttons too.
				 */
				this.addFirst.setEnabled(true);
				this.addBefore.setEnabled(false);
				this.addAfter.setEnabled(false);
				this.moveDown.setEnabled(false);
				this.moveUp.setEnabled(false);
				this.edit.setEnabled(false);
				this.delete.setEnabled(false);
			}else{
				/* If there are Branches on the Highway, the user is only enabled to add new Branches after or before
				 * an existing Branch. So the buttons to add before and after are enabled, while the button to add a
				 * first Obstacle is disabled.  Similar cases apply to the other buttons too.
				 */
				this.addFirst.setEnabled(false);
				this.addBefore.setEnabled(true);
				this.addAfter.setEnabled(true);
				this.moveDown.setEnabled(true);
				this.moveUp.setEnabled(true);
				this.edit.setEnabled(true);
				this.delete.setEnabled(true);
			}
		}
	}
	
	/**
	 * The action listener
	 */
	public void actionPerformed(ActionEvent e){
		
		// Switch between editing Branches and Obstacles
		if(e.getSource() == this.switcher){
			this.mode = !this.mode; // Negate the mode (switch to the opposite one)
			this.validateScreen(); // Validate the screen to display the current mode
		}
		
		// The user pressed the ADD FIRST button.
		if(e.getSource() == this.addFirst){
			// Depending on the mode, either add the appropriate Branch or the appropriate Obstacle.
			if(this.mode){
				this.parent.addObstacle(this.highway, 0);
			}else{
				this.parent.addBranch(this.highway, 0);
			}
		}
		
		/*
		 * The operations edit, delete, move up, move down, add before and add after are only available if
		 * there is a selected Branch or obstacle. So before these operations are executed, check whether
		 * there is an Obstacle or Branch selected, for the appropriate mode.
		 */
		if((this.obstacleTable.getSelectedRow() != -1 && this.mode) || (this.branchTable.getSelectedRow() != -1 && !this.mode)){
			
			// The user pressed the EDIT button.
			if(e.getSource() == this.edit){
				// Depending on the mode, either edit the appropriate Branch or the appropriate Obstacle.
				if(this.mode){
					this.parent.edit(this.highway, this.highway.getObstacle(this.obstacleTable.getSelectedRow()));
				}else{
					this.parent.edit(this.highway, this.highway.getBranch(this.branchTable.getSelectedRow()));
				}
			}
			
			// The user pressed the ADD BEFORE button.
			if(e.getSource() == this.addBefore){
				// Depending on the mode, either add a Branch or an Obstacle to the appropriate index.
				if(this.mode){
					this.parent.addObstacle(this.highway, this.obstacleTable.getSelectedRow());
				}else{
					this.parent.addBranch(this.highway, this.branchTable.getSelectedRow());
				}
			}

			// The user pressed the ADD AFTER button.
			if(e.getSource() == this.addAfter){
				// Depending on the mode, either add a Branch or an Obstacle to the appropriate index.
				if(this.mode){
					this.parent.addObstacle(this.highway, this.obstacleTable.getSelectedRow() + 1);
				}else{
					this.parent.addBranch(this.highway, this.branchTable.getSelectedRow() + 1);
				}
			}

			// The user pressed the DELETE button.
			if(e.getSource() == this.delete){
				// Depending on the mode, either delete the appropriate Branch or the appropriate Obstacle.
				if(this.mode){
					this.parent.deleteObstacle(this.highway, this.highway.getObstacle(this.obstacleTable.getSelectedRow()));
				}else{
					this.parent.deleteBranch(this.highway, this.highway.getBranch(this.branchTable.getSelectedRow()));
				}
			}
			
			// The user pressed the MOVE UP button.
			if(e.getSource() == this.moveUp){
				int num; // Prepare an integer.
				// Depending on the mode, either swap the appropriate Branches or the appropriate Obstacles.
				if(this.mode){
					num = this.obstacleTable.getSelectedRow(); // Get the index of the selected Obstacle.
					if(num > 0){ // If the selected Obstacle is not the very first, continue.
						
						// Swap the selected Obstacle, with the Obstacle one index lower than it.
						this.parent.swap(this.highway.getObstacle(num - 1), this.highway.getObstacle(num), this.highway);
						
						// Make sure the moved Obstacle is selected (for user-friendliness).
						this.obstacleTable.getSelectionModel().setSelectionInterval(num - 1, num - 1);
						
						// Action complete - Return.
						return;
					}else{
						/*
						 * If the selected Obstacle is the very first Obstacle, it cannot be moved up,
						 * hence inform the user of this error.
						 */
						Tools.error("Incorrect obstacle selected for this operation.", this);
					}
				}else{
					num = this.branchTable.getSelectedRow(); // Get the index of the selected Branch.
					if(num > 0){ // If the selected Branch is not the very first, continue.
						
						// Swap the selected Branch, with the Branch one index lower than it.
						this.parent.swap(this.highway.getBranch(num - 1), this.highway.getBranch(num), this.highway);
						
						// Make sure the moved Branch is selected (for user-friendliness).
						this.branchTable.getSelectionModel().setSelectionInterval(num - 1, num - 1);
						
						// Action complete - Return.
						return;
					}else{
						/*
						 * If the selected Branch is the very first Branch, it cannot be moved up, hence inform
						 * the user of this error.
						 */
						Tools.error("Incorrect obstacle selected for this operation.", this);
					}
				}
			}
			
			// The user pressed the MOVE DOWN button.
			if(e.getSource() == this.moveDown){
				int num; // Prepare an integer.
				// Depending on the mode, either swap the appropriate Branches or the appropriate Obstacles.
				if(this.mode){ // Get the index of the selected Obstacle.
					num = this.obstacleTable.getSelectedRow();
					 // If the selected Obstacle is not the very last, continue.
					if(num >= 0 && num < this.highway.getObstacles()-1){
						
						// Swap the selected Obstacle, with the Obstacle one index above it.
						this.parent.swap(this.highway.getObstacle(num), this.highway.getObstacle(num + 1), this.highway);
						
						// Make sure the moved Obstacle is selected (for user-friendliness).
						this.obstacleTable.getSelectionModel().setSelectionInterval(num + 1, num + 1);

						// Action complete - Return.
						return;
					}else{
						/*
						 * If the selected Obstacle is the very last Obstacle, it cannot be moved up,
						 * hence inform the user of this error.
						 */
						Tools.error("Incorrect obstacle selected for this operation.", this);
					}
				}else{
					num = this.branchTable.getSelectedRow(); // Get the index of the selected Branch.
					
					 // If the selected Branch is not the very last, continue.
					if(num >= 0 && num < this.highway.getBranches()-1){
						
						// Swap the selected Branch, with the Branch one index above it.
						this.parent.swap(this.highway.getBranch(num), this.highway.getBranch(num + 1), this.highway);
						
						// Make sure the moved Branch is selected (for user-friendliness).
						this.branchTable.getSelectionModel().setSelectionInterval(num + 1, num + 1);

						// Action complete - Return.
						return;
					}else{
						/*
						 * If the selected Branch is the very last Branch, it cannot be moved up, hence inform
						 * the user of this error.
						 */
						Tools.error("Incorrect obstacle selected for this operation.", this);
					}
				}
			}
		}
	}
}
package dossier;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * Class extends the JFrame and implements a new method centerPosition() that centers the JFrame
 * on the screen.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class MyFrame extends JFrame{
	private static final long serialVersionUID = -5958773787090412521L;

	/**
	 * Constructor creates the class.
	 * @param text The name of the JFrame.
	 */
	public MyFrame(String text){
		super(text); // Call to the constructor of the superclass.
	}
	
	/**
	 * Centers the window on the screen.
	 */
	public void centerPosition(){
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        // Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;
        // Move the window
        this.setLocation(x, y);
	}
}

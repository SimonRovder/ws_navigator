package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
/**
 * Class that displays basic information about the program.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class AboutProgram extends BasePanel implements ActionListener{
	private static final long serialVersionUID = -7429387320810080510L;

	/* Pointer to the Guru class that created this class. (Used to call
	 * methods of Guru).
	 */
	private Guru guru;
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton back;
	
	/**
	 * Constructor saves the pointer passed in the parameters to the
	 * object variable and creates the GUI.
	 * 
	 * @param guru The Guru class that created this class.
	 */
	public AboutProgram(Guru guru){
		super(); // Call to the constructor of the superclass.
		
		// Save the pointers passed in the parameters as the object variables.
		this.guru = guru;
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.back = new MyButton("Back to Main Menu", 247, 472, 300, 30, this);
		this.add(this.back);
		MyLabel label = new MyLabel(null, 0,0,794,572);
		label.setIcon(new ImageIcon(Tools.resourceLink + "aboutProgram.jpg"));
		this.add(label);
	}
	
	/**
	 * The action listener
	 */
	public void actionPerformed(ActionEvent e) {
		/*
		 *  As the only button that uses this action listener is the back button,
		 *  once pressed, simply switch the screen back to the main menu.
		 */
		this.guru.setScreen(Screens.MAIN_MENU);
	}
}
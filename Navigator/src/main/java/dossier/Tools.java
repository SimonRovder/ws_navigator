package dossier;

import java.awt.Component;

import javax.swing.JOptionPane;

/**
 * The Tools class is a class of static content, mainly simple algorithms, but also some variables.
 * These can be used everywhere in the program.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class Tools {
	
	// Standard options for a type of Obstacle.
	public static final String resourceLink = "src/main/resources/data/";
	public static final String fileDataLink = "src/main/resources/FileData";
	
	// Standard options for a type of Obstacle.
	public static final Object options[] = {"MOST","NAD"};
	
	// Standard options for the direction of travel.
	public static final Object directions[] = {"Up the Highway","Down the Highway"};
		
	// Email address that is to be shown in the event of an extreme failure.
	public static final String address = "simon.rovder@gmail.com";
	
	// The path to a backup backup file, used when loading a backup and making a backup in case the loaded backup is wrong.
	public static final String backupFileName = "BACKUPFILE_x3sP789ortGueSS.BACKUP";
	
	/**
	 * Extracts information from a Random Access File record format String and creates an
	 * Obstacle with that information in it.
	 * @param s The String containing the record.
	 * @param index The index of the Record in the Random Access File.
	 * @return The parsed Obstacle.
	 */
	public static Obstacle parseObstacle(String s, int index){
		if(s != null){ // Check the provided String is not null
			try{
				// Split the provided String at the semicolons.
				String obs[] = s.split(";");
				
				// Transform the String containing information about the activity of the restriction into an array of characters.
				char[] set = obs[15].toCharArray();
				
				// Create the two Restrictions of an Obstacle from the values in the two arrays (set and obs).
				Restriction left = new Restriction(obs[3], getDouble(obs[4]), getDouble(obs[5]), getDouble(obs[6]), getDouble(obs[7]), getDouble(obs[8]), (set[0] == '1'), (set[1] == '1'));
				Restriction right = new Restriction(obs[9], getDouble(obs[10]), getDouble(obs[11]), getDouble(obs[12]), getDouble(obs[13]), getDouble(obs[14]), (set[2] == '1'), (set[3] == '1'));
				
				// Create the Obstacle from the values in the two arrays (set and obs) and the two restrictions.
				Obstacle obstacle = new Obstacle(index, obs[0], Double.parseDouble(obs[1]), obs[2], left, right);
				
				return obstacle; // Return the new Obstacle. 
				
			}catch(Exception ex){
				// Return null if anything goes wrong of anything is of wring format.
				return null;
			}
		}		
		return null; // If the provided String was null, return null
	}
	
	public static Branch parseBranch(String s, int index){
		if(s != null){ // Check the provided String is not null
			try{
				// Split the provided String at the semicolons.
				String obs[] = s.split(";");
				if(obs.length == 10){
					
					// Transform the String containing information about the activity of the restriction into an array of characters.
					char[] set = obs[9].toCharArray();
					
					// Create the Restriction for the Branch from the values in the two arrays (set and obs).
					Restriction restriction = new Restriction(obs[3], getDouble(obs[4]), getDouble(obs[5]), getDouble(obs[6]), getDouble(obs[7]), getDouble(obs[8]), (set[0] == '1'),(set[1] == '1'));
					
					// Create the Branch from the values in the two arrays (set and obs) and the restriction.
					Branch branch = new Branch(index, obs[0], Double.parseDouble(obs[1]), obs[2], restriction);
					
					
					return branch;
				}
			}catch(Exception ex){
				// Return null if anything goes wrong of anything is of wring format.
				return null;
			}
		}
		return null; // If the provided String was null, return null
	}
	
	/**
	 * Transforms the provided Obstacle into the form in which it is stored in the backup file
	 * @param o The Obstacle that is to be transformed.
	 * @return Branch in the form in which it is stored on Disk
	 */
	public static String parseText(Obstacle o){
		// Return all the values in the provided Obstacle in one String, all separated by semicolons.
		return o.getType() + ";" + o.getKm() + ";" + o.getDescription() + ";" + o.getLeft().getId() + ";" + o.getLeft().getSeparation() + ";" + o.getLeft().getHeight() + ";" + o.getLeft().getWeight(0) + ";" + o.getLeft().getWeight(1) + ";" + o.getLeft().getWeight(2) + ";" + o.getRight().getId() + ";" + o.getRight().getSeparation() + ";" + o.getRight().getHeight() + ";" + o.getRight().getWeight(0) + ";" + o.getRight().getWeight(1) + ";" + o.getRight().getWeight(2) + ";" + o.getLeft().getActivity() + o.getRight().getActivity();
	}
	
	/**
	 * Transforms the provided Branch into the form in which it is stored in the backup file
	 * @param b The Branch that is to be transformed.
	 * @return Branch in the form in which it is stored on Disk
	 */
	public static String parseText(Branch b){
		// Return all the values in the provided Branch in one String, all separated by semicolons.
		return "VETVA;" + b.getKm() + ";" + b.getDescription() + ";" + b.getRestriction().getId() + ";" + b.getRestriction().getSeparation() + ";" + b.getRestriction().getHeight() + ";" + b.getRestriction().getWeight(0) + ";" + b.getRestriction().getWeight(1) + ";" + b.getRestriction().getWeight(2) + ";" + b.getRestriction().getActivity();
	}
	
	/**
	 * Checks whether the string corresponds to a bridge or branch or above bridge
	 * @param s Obstacle in the form in which it is stored on Disk
	 * @return A String containing the type name.
	 */
	public static String getType(String s){
		String[] test = s.split(";");
		/*
		 * The type is always in front of the first semicolon in the string. So once the string is split at the
		 * semicolons, the zeroth String in the resulting array will contain it.
		 */
		return test[0]; // Return the zeroth String in the array. 
	}
	
	/**
	 * Transforms the provided String from the values that are displayed,
	 * to the values that are to be stored in the main memory.
	 * This can be a number for numeric Strings, or:
	 * 
	 * -1 if the String contains "???" - the value is unknown
	 * -2 if the String contains non-numeric characters
	 * 
	 * @param s String with a number.
	 * @return double of the corresponding value to the String text.
	 */
	public static double getDouble(String s) throws Exception{
		if(s.equals("???")){
			return -1; // Only return -1 if the String is "???"
		}else{
			return Double.parseDouble(s);
		}
	}
	
	/**
	 * Transforms the provided double from the values stored
	 * in the main memory, to the values that are to be displayed.
	 * This can be a String with numbers, or:
	 * 
	 * "???" if the double is -1 - the value is unknown
	 * "---" if the double is -2 - the value is inactive
	 * 
	 * @param in Double of certain value.
	 * @return Resulting String for that value.
	 */
	
	public static String isUnknown(double in){
		if(in == -1){
			// If the double is -1, the value is unknown. Hence return "???".
			return "???";
		}
		if(in == -2){
			// If the double is -2, the value is inactive. Hence return "---".
			return "--";
		}
		// In any other case, return the double itself.
		return "" + in;
	}
	
	/**
	 * Checks whether the provided string is of numeric form or in
	 * the form of "???". If neither is true, returns false
	 * @param s
	 * @return
	 */
	
	public static boolean isValidValue(String s){  
		if(s.equals("???")){
			// "???" is considered a valid value
			return true;
		}
		try{
			Double.parseDouble(s);
		}catch(NumberFormatException ex){
			// If the String does not contain a numeric text or "???", it is not valid - return false.
			return false;
		}
		// If the String was invalid, false would already have been returned. So the String is valid - return true. 
		return true;
	}
	
	/**
	 * Prints the string in a console
	 * @param s
	 */
	public static void p(String s){
		System.out.println(s);
	}
	
	/**
	 * evaluates the supplied string and returns an array of the values according to the signs "+" and "x"
	 * and compares the result with the total provided by the user.
	 * For example:
	 * 2+2+3x5+8 --> 2,2,5,5,5,8
	 * @param text The string
	 * @param total The sum of the values in the provided String (Redundancy check)
	 * @return array The array of doubles extrapolated from the provided String
	 */
	
	/*
	 * MASTERY FACTOR 9 - Parsing a text file or other data stream.   
	 */
	public static double[] toField(String text, double total){
		/* The "+" sign has a lower priority, so the String must be split by it first, to obtain the subset
		 * of Strings containing only "x". 
		 */
    	String[] base = text.split("\\+");
    	
    	String[] temp; 			// Prepare another array.
    	String changed = "";  	// Prepare the empty String for partially assessed data.
/*
 * MASTERY FACTOR 11 -- 7 - Nested loops.
 */
    	for(int i = 0; i < base.length; i++){ // Repeat this process for every subset value in the array
    		
    		//Split the subset String at the "x"
    		temp = base[i].split("x");
    		
    		if(temp.length > 1){
    			/* If the length of the split subset String is greater than 1, it
    			 * contained an "x" and must be processed.
    			 */
    			for(int j = Integer.parseInt(temp[0].trim()); j > 0; j--){
    				/*
    				 * This loop appends the multiplied value (second value of the "AxB" subset) to the processed String
    				 * as many times as it is multiplied by. So "2x4" would be transformed into ;4;4
    				 */
    				changed = changed + ";" + temp[1].trim();
    			}
    		}else{
    			/*
    			 * If the length of the split subset String is less than 1, it
    			 * did not contain an "x" and must hence be appended to the end of the
    			 * processed String only once.
    			 */
				changed = changed + ";" + temp[0].trim();
    		}
    	}
    	
    	// Split the processed String at the semicolons to obtain the numeric values only
    	base = changed.split(";");
    	
    	/* Prepare an array of doubles for the numeric values from the processed String.
    	 * (the String has an additional semicolon, for the length of the array of doubles
    	 * is smaller by one compared to that of the Strings)
    	 */
    	double[] ret = new double[base.length - 1];
    	double count = 0; // Prepare to sum up the values
    	for(int i = 0; i < base.length - 1; i++){
    		/*
    		 * Parse all the now numeric Strings to doubles and fill the array with them
    		 * (in the correct order)
    		 */
    		ret[i] = Double.parseDouble(base[i + 1]);
    		count = count + ret[i]; // Sum up the values while parsing them
    	}
    	
    	if(count == total){ // Perform a redundancy check of whether the values add up correctly
    		return ret; // Return the array if they do
    	}else{
    		return null; // return null if they do not (user has probably accidentally input wrong values)
    	}
    }
	
	/**
	 * Displays an error message
	 * @param text Displayed Text
	 * @param o Component
	 */
	public static void error(String text, Object o){
    	JOptionPane.showMessageDialog((Component) o, text, "Error", JOptionPane.ERROR_MESSAGE);
    }
	
	/**
	 * Displays a message
	 * @param text Displayed Text
	 * @param o Component
	 */
	public static void message(String text, Object o){
    	JOptionPane.showMessageDialog((Component) o, text, "Message", JOptionPane.PLAIN_MESSAGE);
    }
    
	/**
	 * Displays a warning message
	 * @param text Displayed Text
	 * @param o Component
	 */
    public static void warning(String text, Object o){
    	JOptionPane.showMessageDialog((Component) o, text, "Warning", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Allows the user to select from a selection
     * @param options Array of options
     * @param initial Initially selected option
     * @param text Displayed Text
     * @return Selection (null if cancel was pressed)
     */
    public static String select(Object[] options, String initial, String text) {
    	// Display the selection window
		Object selection = JOptionPane.showInputDialog(null, text,	"Question", JOptionPane.QUESTION_MESSAGE, null, options, initial);
		if(selection != null){
			return selection.toString(); // User has pressed OK - return the selection
		}else{
			return null; // User has pressed CANCEL - return null
		}
    }
    
    /**
     * This method allows the user to confirm an action (yes/no window)
     * @param text is the question displayed in the window
     * @param o Component
     * @return the boolean expression for the users choice
     */
    public static boolean confirm(String text, Object o){
    	// Display the confirm dialog and save the answer in an Integer.
    	int response = JOptionPane.showConfirmDialog(null, text, "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
	    
    	// Returns true if the integer corresponds to the user clicking YES.
    	return (response == JOptionPane.YES_OPTION);
    }
    
    /**
     * This method allows for the user to provide input.
     * @param question The question the window displays.
     * @param text The text the window displays in the answer field.
     * @return The written text (null if cancel was pressed)
     */
    public static String userInput(String question, String text){
    	// Show the input dialog and save the input in a String
    	String s = JOptionPane.showInputDialog (question, text);
    	
    	return s; // Return the String (null if the user pressed CANCEL)
    }
    
    /**
     * This method counts the length of the given string in bytes of UTF8 encoding
     * @param s String to assess
     * @return The length
     */
    public static int getUTFLength(String s){
        try{
            byte[] utf8Bytes = s.getBytes("UTF8");	// Get an array of Bytes from the String
            return utf8Bytes.length;				// Return the length of the array
        }catch(Exception ex){
            return new Integer(-1); 				// Return -1 if something went wrong
        }
    }
    
    /**
     * Validates all the Restrictions of all the Obstacles and Branches on all the Highways in the provided
     * HighwaySystem.
     *  
     * @param highwaySystem The HighwaySystem that is to be validated.
     * @return true if the HighwaySystem is valid, false if the HighwaySystem os not valid.
     */
	public static boolean validateHighwaySystem(HighwaySystem highwaySystem) {
		// Get the first Highway in the HighwaySystem.
		Highway highway = (Highway) highwaySystem.getFirst();
		
		// Prepare an Obstacle and a Branch.
		Obstacle obstacle;
		Branch branch;
		// Iterate through all the Highways in the HighwaySystem
		while(highway != null){
			// Get the first Obstacle on the current Highway.
			obstacle = highway.getFirstObstacle();
			// Iterate through all the Obstacles on the Highway.
			while(obstacle != null){
				
				// Validate both the restrictions and return false if either one of them is invalid.
				if(!Tools.validateRestriction(obstacle.getLeft())) return false;
				if(!Tools.validateRestriction(obstacle.getRight()))	return false;
				
				// Continue iteration with the next Obstacle.
				obstacle = (Obstacle) obstacle.getNext();
			}
			branch = highway.getFirstBranch();
			// Get the first Obstacle on the current Highway.
			while(branch != null){
				
				// Validate the restriction and return false if it is invalid.
				if(!Tools.validateRestriction(branch.getRestriction())) return false;
				
				// Continue iteration with the next Branch.
				branch = (Branch) branch.getNext();
			}
			// Continue iteration with the next Highway.
			highway = (Highway) highway.getNext();
		}
		// If false was not returned by this point, the HighwaySystem is valid. Hence return true.
		return true;
	}

	/**
	 * Validates whether the provided Restriction is not faulty (activity corresponds to values).
	 * Also removes the pointed brackets from the ID.
	 * 
	 * @param restriction The Restriction that is to be validated.
	 * @return true if the Restriction is valid, false if it is not.
	 */
	private static boolean validateRestriction(Restriction restriction) {
		// Check whether the Restriction is not null.
		if(restriction != null){
			// Check whether if the height limitation of this Restriction is active, is there a valid value for it.
			if(restriction.isUseHeights() && restriction.getHeight() < -2){
				// If the activity and value of the height limitation do not correspond, return null.
				return false;
			}
			// Check whether if the weight limitations of this Restriction are active, are there valid values for them.
			if(restriction.isUseWeights() && (restriction.getSeparation() < -2 || restriction.getWeight(0) < -2 ||
					restriction.getWeight(1) == -2 || restriction.getWeight(2) == -2)){
				// If the activity and values of the weight limitations do not correspond, return false.
				return false;
			}
	        restriction.setId(Tools.removeInvalidCharacters(restriction.getId()));
			return true;
		}else{
			// If the Restriction is null, return false;
			return false;
		}
	}
	
	/**
	 * Removes all pointed brackets from the provided String and returns a String without them.
	 * @param string The String that needs to be cleaned of all "<" and ">"
	 * @return The cleaned String.
	 */
	public static String removeInvalidCharacters(String string){
		// Remove all occurrences of "<".
		string = string.replace("<","");
		// Remove all occurrences of ">".
		string = string.replace(">","");
		// Remove all occurrences of ";".
		string = string.replace(";","");
		// Return the new String.
		return string;
	}
}

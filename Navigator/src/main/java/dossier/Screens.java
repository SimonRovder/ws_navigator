package dossier;

/**
 * The enum class containing the enumerated values of all possible screens
 * that can be displayed in Guru.
 * (class used to specify the required screen)
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public enum Screens {
	MAIN_MENU,
	EDITOR,
	TRIP_SETUP,
	ROUTE_PLANNER,
	MANAGEMENT,
	OUTPUT_SCREEN,
	ABOUT_PROGRAM
}
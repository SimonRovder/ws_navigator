package dossier;

import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * This class has the purpose of making JButton creation easier. It extends a JButton
 * and provides a more efficient constructor.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class MyButton extends JButton{
	private static final long serialVersionUID = -7430607734726100563L;

	/**
	 * Constructor that sets up the JButton
	 * @param text Text to be written on the JButton
	 * @param xPos X Position of the JButton
	 * @param yPos Y Position of the JButton
	 * @param width Width of the JButton
	 * @param height Height of the JButton
	 * @param o The ActionListener of the JButton
	 */
	public MyButton(String text, int xPos, int yPos, int width, int height, Object o){
		super(text); // Call to the Constructor of the super class, passing the text for the JButton.
		this.setBounds(xPos,yPos,width,height);		// Setting the Bounds of the JButton to those provided.
		this.addActionListener((ActionListener) o);	// Setting the ActionListener of the JButton to that provided.
		this.setVisible(true);	
		this.setToolTipText(text); // Making the JButton visible.
	}
}
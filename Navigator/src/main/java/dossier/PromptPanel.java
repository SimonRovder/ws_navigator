package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

/**
 * This class contains the GUI Components that will enable the user to edit a single Restriction.
 * It also contains the algorithm that parses the data in the fields into the restriction it
 * is editing. It extends the JPanel, as it is to be placed onto a JFrame.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class PromptPanel extends JPanel implements ActionListener{
	private static final long serialVersionUID = -499636011425977051L;

	// The Restriction that this PromptPanel is editing.
	private Restriction restriction;
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	
	private JCheckBox cHeight;
	private JCheckBox cWeight;
	
	private MyTextField id;
	private MyTextField separation;
	private MyTextField height;
	private MyTextField w1;
	private MyTextField w2;
	private MyTextField w3;
	
	/**
	 * Set up the GUI Components
	 * @param title The title that is to be displayed on the JPanel
	 */
	public PromptPanel(String title){
		super(); // Call to the constructor of the superclass.
		
		this.setLayout(null); // Set the layout to null.
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the JPanel
		 */
		
		this.add(new MyLabel("<html><h1>" + title + "</h1></html>", 140, 5, 150, 30));
		
		this.cHeight = new JCheckBox("Height: ");
		this.cHeight.setBounds(10, 80, 100, 20);
		this.cHeight.addActionListener(this);
		
		this.cWeight = new JCheckBox("Normal: ");
		this.cWeight.setBounds(10, 115, 100, 20);
		this.cWeight.addActionListener(this);

		this.id = new MyTextField(135, 45, 180, 20);
		this.height = new MyTextField(135, 80, 180, 20);
		this.add(new MyLabel("m", 320, 80, 25, 20));
		
		this.w1 = new MyTextField(135, 115, 180, 20);
		this.add(new MyLabel("t", 320, 115, 25, 20));
		
		this.w2 = new MyTextField(135, 140, 180, 20);
		this.add(new MyLabel("t", 320, 140, 25, 20));
		
		this.w3 = new MyTextField(135, 170, 180, 20);
		this.add(new MyLabel("t", 320, 170, 25, 20));
		
		this.separation = new MyTextField(135, 200, 180, 20);
		this.add(new MyLabel("m", 320, 200, 25, 20));
		
		this.add(new MyLabel("ID:", 30, 45, 100, 20));
		this.add(this.cHeight);
		this.add(this.cWeight);
		this.add(new MyLabel("Separation: ", 32, 200, 100, 20));
		this.add(new MyLabel("Exclusive:", 32, 140, 100, 20));
		this.add(new MyLabel("Extraordinary:", 32, 170, 100, 20));
		
		this.add(this.id);
		this.add(this.separation);
		this.add(this.height);
		this.add(this.w1);
		this.add(this.w2);
		this.add(this.w3);
		

		// Refresh the fields, to make sure the appropriate fields are enabled.
		this.refreshEnabled();
	}

	/**
	 * Sets up the PromptPanel to show the values of the passed Restriction.
	 * @param restriction The Restriction that is to be shown in the fields.
	 */
	public void setPanel(Restriction restriction){
		// Save the pointer to the Restriction passed in the parameters as the object variable.
		this.restriction = restriction;
		
		// Set the texts in the fields to show the values in the Restriction.
		this.id.setText(restriction.getId());
		this.separation.setText(Tools.isUnknown(restriction.getSeparation()));
		this.height.setText(Tools.isUnknown(restriction.getHeight()));
		this.w1.setText(Tools.isUnknown(restriction.getWeight(0)));
		this.w2.setText(Tools.isUnknown(restriction.getWeight(1)));
		this.w3.setText(Tools.isUnknown(restriction.getWeight(2)));
		
		// Set the active limitations posed by the restriction to be shown as such via JCheckBoxes.
		this.cHeight.setSelected(restriction.isUseHeights());
		this.cWeight.setSelected(restriction.isUseWeights());

		// Refresh the fields, to make sure the appropriate fields are enabled.
		this.refreshEnabled();
	}
	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		/*
		 * Whenever the user makes any action, make sure the appropriate fields are enabled.
		 * (The user may have unchecked one of the JCheckBoxes)
		 */
		this.refreshEnabled();
	}
	
	/**
	 * Enables the appropriate fields, depending on whether that particular limitation was checked as active
	 * by the corresponding JCheckBox.
	 */
	public void refreshEnabled(){
		/*
		 * Set the ENABLED feature of each field to the CHECKED status of its corresponding JCheckBox.
		 * Thus every field that was checked as active, will also be enabled.
		 */
		this.height.setEnabled(this.cHeight.isSelected());
		this.w1.setEnabled(this.cWeight.isSelected());
		this.w2.setEnabled(this.cWeight.isSelected());
		this.w3.setEnabled(this.cWeight.isSelected());
		this.separation.setEnabled(this.cWeight.isSelected());
	}
	
	/**
	 * Checks whether correct (numeric) values were supplied in the active fields.
	 * @return true if the values were numeric, false if the values were not numeric.
	 */
	public boolean check(){
		
		/*
		 * If the weight limitation was selected as active, check whether the fields corresponding to it
		 * are filled with numeric values only.
		 */
		if(this.cWeight.isSelected()){
			if(!Tools.isValidValue(this.w1.getText().trim()) || !Tools.isValidValue(this.w2.getText().trim()) || !Tools.isValidValue(this.w3.getText().trim()) || !Tools.isValidValue(this.separation.getText().trim())){
				// If either one of the values was not numeric, return false.
				return false;
			}
		}
		
		/*
		 * If the height limitation was selected as active, check whether the fields corresponding to it
		 * are filled with numeric values only.
		 */
		if(this.cHeight.isSelected()){
			if(!Tools.isValidValue(this.height.getText().trim())){
				// If the value was not numeric, return false.
				return false;
			}
		}
		
		/*
		 * If this point was reached, all the active fields were only filled with numeric values.
		 * Hence return true.
		 */
		return true;
	}
	
	
	/**
	 * Parses the data provided in the fields as the Restriction that is loaded into the PromptPanel.
	 * @return The updated Restriction.
	 */
	public Restriction getRestriction(){
		/*
		 * Before the program parses the text in the fields, check whether the data provided in the text
		 * fields are of numeric value. Only in such case, proceed with the parsing.
		 */
		if(this.check()){
			try{	
				/*
				 * Save the set state (active or inactive) of the height limitation in the Restriction.
				 */
				if(this.cHeight.isSelected()){
					// The height limitation was set to active.
					
					// Set the height limitation in the loaded Restriction to active.
					this.restriction.setUseHeights(true);
					
					// Set the provided value of the maximum height to the loaded Restriction.
					this.restriction.setHeight(Tools.getDouble(this.height.getText()));
				}else{
					// The height limitation was set to inactive.
					
					// Set the height limitation in the loaded Restriction to inactive.
					this.restriction.setUseHeights(false);
					
					// Set the value of the maximum height in the loaded Restriction to -2 (inactive).
					this.restriction.setHeight(-2);
				}
				
				
				/*
				 * Save the set state (active or inactive) of the weight limitation in the Restriction.
				 */
				if(this.cWeight.isSelected()){
					// The weight limitations were set to active.
					
					// Set the height limitation in the loaded Restriction to active.
					this.restriction.setUseWeights(true);
					
					// Set the provided value of the new length of the Restriction to the loaded Restriction.
					this.restriction.setSeparation(Tools.getDouble(this.separation.getText()));
					
					/* Prepare a flag boolean to check for whether the weight limits are in order
					 * from smallest to greatest.
					 */
					boolean weightCheck = true;
					
					/* Check whether the first weight limit is smaller than or equal to the second weight
					 * limit. Do not check, if either of them is unknown.
					 */
					if(!this.w1.getText().equals("???") && !this.w2.getText().equals("???")){
						if(!(Tools.getDouble(this.w1.getText()) <= Tools.getDouble(this.w2.getText()))){
							/* If this occurs, the first and second weight limits are known, but are nor in
							 * the correct order. Hence set the flag boolean to false.
							 */
							weightCheck = false;
						}
					}
					
					/* Check whether the second weight limit is smaller than or equal to the third weight
					 * limit. Do not check, if either of them is unknown.
					 */
					if(!this.w2.getText().equals("???") && !this.w3.getText().equals("???")){
						if(!(Tools.getDouble(this.w2.getText()) <= Tools.getDouble(this.w3.getText()))){
							/* If this occurs, the second and third weight limits are known, but are nor in
							 * the correct order. Hence set the flag boolean to false.
							 */
							weightCheck = false;
						}
					}
					
					/* Check whether the first weight limit is smaller than or equal to the third weight
					 * limit. Do not check, if either of them is unknown.
					 */
					if(!this.w1.getText().equals("???") && !this.w3.getText().equals("???")){
						if(!(Tools.getDouble(this.w1.getText()) <= Tools.getDouble(this.w3.getText()))){
							/* If this occurs, the first and third weight limits are known, but are nor in
							 * the correct order. Hence set the flag boolean to false.
							 */
							weightCheck = false;
						}
					}
					
					// Check the flag boolean.
					if(weightCheck){
						/*
						 * If the flag boolean remained true, all the weight limits are in the correct order
						 * and are to be written into the loaded Restriction.
						 */
						this.restriction.setWeight(0,Tools.getDouble(this.w1.getText()));
						this.restriction.setWeight(1,Tools.getDouble(this.w2.getText()));
						this.restriction.setWeight(2,Tools.getDouble(this.w3.getText()));
					}else{
						/*
						 * If the flag boolean turned false, one or more of the weight limits were in an
						 * incorrect order. Hence return false.
						 */
						return null;
					}
				}else{
					// The weight limitations were set to inactive.
					
					// Set the weight limitations as inactive in the loaded Restriction.
					this.restriction.setUseWeights(false);
					
					// Set the values of the weight limitations in the loaded Restriction to -2 (inactive).
					this.restriction.setWeight(0,-2);
					this.restriction.setWeight(1,-2);
					this.restriction.setWeight(2,-2);
					this.restriction.setSeparation(-2);
				}
				
				// Set the ID of the loaded Restriction to the ID in the ID field (remove invalid characters).
				this.restriction.setId(Tools.removeInvalidCharacters(this.id.getText()));
				
				/*
				 * If the program got to this point, it means the loading of the Restriction was successful
				 * and the Restriction can be returned.
				 */
				return this.restriction;
			}catch(Exception ex){
				/*
				 * If this occurs, the parsing of the data in the fields to the loaded Restriction
				 * failed for some reason. The most likely reason is a number format exception,
				 * which should have been checked for before the parsing took place. Hence the error
				 * must have been something else.
				 * 
				 * Inform the user of this error.
				 */
				Tools.error("An internal error occured in the program." +
						"The Restriction might now be damaged.\n\n" + ex.toString(), this);
				// And return null.
				return null;
			}
		}else{
			/* This occurs if any of the values provided in the fields is not of numeric value.
			 * In such situation, return null.
			 */
			return null;
		}
	}
	
	
	/**
	 * Finds the total text this restriction will take in a random access file.
	 * @return The total text this restriction will take in a random access file.
	 */
	
	public String getTotalText() {
		/*
		 * Create the String totalText, in which all the Text will be added together. Fill it with the
		 * ID from the field and "00", representing the two characters that store activity in the
		 * random access file.
		 */
		String totalText = this.id.getText().trim() + "00";
		
		
		// Check whether the height limitation is set to active.
		if(this.cHeight.isSelected()){
			// If it is active, add the text in its field to the totalText String.
			totalText = totalText + this.height.getText().trim();
		}else{
			// If it is not active, add "-2" to the totalText String (representing an inactive value).
			totalText = totalText + "-2";
		}
		

		// Check whether the weight limitations are set to active.
		if(this.cWeight.isSelected()){
			// If they is active, add the text in their text fields to the totalText String.
			totalText = totalText + this.separation.getText().trim();
			totalText = totalText + this.w1.getText().trim();
			totalText = totalText + this.w2.getText().trim();
			totalText = totalText + this.w3.getText().trim();
		}else{
			/* If they are not active, add "-2-2-2-2" to the totalText String
			 * (representing their inactive values).
			 */
			totalText = totalText + "-2-2-2-2";
		}
		
		// Return the text summed up in one String.
		return totalText;
	}
}

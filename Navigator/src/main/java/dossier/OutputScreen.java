package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 * Class takes care of the GUI for displaying and exporting the output of the processed Trip.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class OutputScreen extends BasePanel implements ActionListener{
	private static final long serialVersionUID = -922903103764856727L;

	// Pointer to the Guru class that created this class.
	private Guru guru;
	
	// Variables used to store the Report and the Trip, the results of which are being displayed on the screen.
	private Report report;
	private Trip trip;
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton backButton;
	private MyButton export;
	private MyButton done;
	private JTextArea textArea;
	private JScrollPane scrollPane;
	private JFileChooser chooser;
	
	/**
	 * Constructor saves the pointer passed in the parameters to the
	 * object variable and creates the GUI along with other necessary classes.
	 * 
	 * @param guru The Guru class that created this class.
	 */
	public OutputScreen(Guru guru){
		super(); // Call to the constructor of the superclass.

		// Save the passed Guru as the object variable.
		this.guru = guru;
		
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.chooser = new JFileChooser();
		this.chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		this.chooser.setMultiSelectionEnabled(false);
		
		this.textArea = new JTextArea();
		this.textArea.setEditable(false);
		this.scrollPane = new JScrollPane(this.textArea);
		this.scrollPane.setBounds(20, 20, 740, 400);
		this.add(this.scrollPane);
		
		this.backButton = new MyButton("Back to route selection",540,440,220,40,this);
		this.add(this.backButton);

		this.export = new MyButton("Export PDF",280,440,220,40,this);
		this.add(this.export);
		
		this.done = new MyButton("Done (Back to Main Menu)",20,440,220,40,this);
		this.add(this.done);
	}
	
	/**
	 * Saves the provided Report and Trip as the object variables and displays the Report on
	 * the Output screen.
	 * 
	 * @param report The Report
	 * @param trip The Trip
	 */
	public void loadScreen(Report report, Trip trip){
		// Save the passed report and Trip as the object variables.
		this.report = report;
		this.trip = trip;
		
		// Load the Trip to the output screen (MyTextArea).
		this.textArea.setText(this.report.toString());
	}

	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == this.backButton){ // The BACK button was clicked.
			// Switch the screen back to the RoutePlanner.
			this.guru.setScreen(Screens.ROUTE_PLANNER);
		}
		
		if(e.getSource() == this.export){ // The EXPORT button was clicked.
			// Enable the user to browse for the directory.
			if(this.chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
				try {
					// Save the Report and trip to an xml file via the FileManager.
					this.guru.fileManager.output(this.report, this.trip);
					// Transform the saved xml files to a PDF file via the PDFPrinter.
					PDFWriter.exportPDF(this.chooser.getSelectedFile().getAbsolutePath());
					// Inform the user that the export is complete.
					Tools.message("Export complete\n\nFile saved to: " + this.chooser.getSelectedFile().getAbsolutePath(), this);
				} catch (Exception ex) {
					Tools.error("Export failed!\n\n" + ex.toString(),this);
				}
			}
		}
		
		if(e.getSource() == this.done){ // The user is done with the Trip.
			// Prompt the user to confirm he wishes to return to the main menu.
			if(Tools.confirm("Once you are done, the information about the vehicle,company and routes will vanish.\nAre you really done?", this)){
				// Exit the OutputScreen and go back to the main menu via the Guru method exitTripSetupMode().
				this.guru.exitTripSetupMode();
			}
		}
	}
}
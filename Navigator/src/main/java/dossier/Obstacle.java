package dossier;

/**
 * This class stores all information about an obstacle.
 * It extends the BasicOB class and hence also extends
 * the Node and can be used as an item in a LinkedList.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class Obstacle extends BasicOB{
	
	// Variable that contains the Restriction posed by the left side of the Obstacle.
	private Restriction left;
	
	// Variable that contains the Restriction posed by the right side of the Obstacle.
	private Restriction right;

	/**
	 * Constructor creates the object and sets the object variables to the pointers passed in the parameters.
	 * 
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 * @param left The Restriction posed by the left side of the Obstacle.
	 * @param right The Restriction posed by the right side of the Obstacle.
	 */
	public Obstacle(int index, String type, double km, String description, Restriction left, Restriction right){
		super(index, type, km, description);
		this.left = left;
		this.right = right;
	}
	
	/**
	 * Saved the main information about the transport taking place.
     * (Saves the parameters as the corresponding object variables)
	 * 
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 * @param left The Restriction posed by the left side of the Obstacle.
	 * @param right The Restriction posed by the right side of the Obstacle.
	 */
	public void setup(int index, String type, double km, String description, Restriction left, Restriction right){
		super.setup(index, type, km, description);
		this.left = left;
		this.right = right;
	}
	
	/**
	 * Getter for the object variable storing the Restriction posed by the left side of this Obstacle.
	 * 
	 * @return The Left Restriction.
	 */
	public Restriction getLeft() {
		return this.left;
	}

	/**
	 * Getter for the object variable storing the Restriction posed by the right side of this Obstacle.
	 * 
	 * @return The Right Restriction.
	 */
	public Restriction getRight() {
		return this.right;
	}
}

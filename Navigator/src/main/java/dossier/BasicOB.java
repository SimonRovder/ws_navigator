package dossier;

/**
 * This class is the basic class that will be extended by the Branch and the Obstacle.
 * It contains the standard variables that are used by both the Branch and the Obstacle
 * and only serves to counteract code redundancy.
 * 
 * (Extends Node as both the Obstacle and Branch will be parts of a LinkedList)
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class BasicOB extends Node{
	
	// String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	private String type;
	
	// String that contains the user-defined description of the Branch or Obstacle
	private String description;
	
	// Location on the Highway (distance from the start of the Highway)
	private double km;
	
	// The index of this Branch or Obstacle in its Random Access File.
	private int index;
	
	/**
	 * Constructor creates the object and sets the object variables to the pointers passed in the parameters.
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 */
	public BasicOB(int index, String type, double km, String description){
		super();
		this.setup(index, type, km, description);
	}
	
	/**
	 * Sets the object variables to the pointers passed in the parameters.
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 */
	public void setup(int index, String type, double km, String description){
		this.index = index;
		this.type = type;
		this.description = description;
		this.km = km;
	}
	
	/**
	 * Getter for the object variable storing the description of the Branch or Obstacle.
	 * 
	 * @return The description of the Branch or Obstacle.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Getter for the object variable storing the location of the Branch or Obstacle.
	 * on the highway.
	 * 
	 * @return The location of the Branch or Obstacle.
	 */
	public double getKm() {
		return this.km;
	}

	/**
	 * Getter for the object variable storing the type of the Branch or Obstacle.
	 * 
	 * @return The type of the Branch or Obstacle.
	 */
	public String getType() {
		return this.type;
	}
	
	/**
	 * Getter for the object variable storing the index of the Branch or Obstacle.
	 * in its Random Access File.
	 * 
	 * @return
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Setter for the object variable storing the index of the Branch or Obstacle.
	 * @param index Index of the Branch or Obstacle
	 */
	public void setIndex(int index) {
		this.index = index;
	}
}
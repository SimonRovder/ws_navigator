package dossier;

import java.io.File;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

/**
 * 
 * Class that manages PDF output.
 * 
 * This class came with the additional libraries, yet I had to incorporate it
 * into my program and make it do what I wanted (name the file as the date,
 * read the correct xml and xsl files...).
 * 
 * @author Freeware and Simon Rovder
 *
 */
/*
 * MASTERY FACTOR 16 - Use of additional libraries.
 */
public class PDFWriter {
	public static void exportPDF(String dir){
		//new PDFWriter();
		try {
			// Setup directories
			File baseDir = new File(Tools.fileDataLink);
			
			// Get the date and time (both are included in the file name, as to not overwrite existing files).
			DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
			Calendar cal = Calendar.getInstance();
			String s = dateFormat.format(cal.getTime());
			
			// Setup input and output files
			File xmlfile = new File(baseDir, "input.xml");
			File xsltfile = new File(baseDir, "DO_NOT_DELETE_ME.xsl");
			File pdffile = new File(dir, "Exported__" + s + ".pdf");
			
			
			// configure fopFactory as desired
			final FopFactory fopFactory = FopFactory.newInstance();
			
			FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
			// configure foUserAgent as desired
			
			// Setup output
			OutputStream out = new java.io.FileOutputStream(pdffile);
			out = new java.io.BufferedOutputStream(out);
			
			try {
				// Construct fop with desired output format
				Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
				
				// Setup XSLT
				TransformerFactory factory = TransformerFactory.newInstance();
				Transformer transformer = factory.newTransformer(new StreamSource(xsltfile));
				
				// Set the value of a <param> in the stylesheet
				//transformer.setParameter("versionParam", "2.0");
				
				// Setup input for XSLT transformation
				Source src = new StreamSource(xmlfile);
				
				// Resulting SAX events (the generated FO) must be piped through to FOP
				Result res = new SAXResult(fop.getDefaultHandler());
				
				// Start XSLT transformation and FOP processing
				transformer.transform(src, res);
			} finally {
			out.close();
			}
			
			} catch (Exception e) {
			e.printStackTrace(System.err);
			System.exit(-1);
			}
	}
}

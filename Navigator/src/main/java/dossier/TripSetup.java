package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Visual class that extends the BasePanel and provides fields for the users input of information regarding the
 * vehicle, company name and date of transport.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class TripSetup extends BasePanel implements ActionListener{
	private static final long serialVersionUID = -863567907297873122L;

	/*
	 * Pointer to the Guru class that created this class
	 * - Used to call methods of Guru
	 */
	private Guru guru;
	
	/*
	 * Pointer to the Trip created in Guru. This class will edit the appropriate information in the Trip
	 * to that provided by the user on this screen.
	 */
	private Trip trip;
	
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton backButton;
	private MyButton next;
	
	private MyTextField companyName;
	private MyTextField dateOfTransport;
	private MyTextField weightSeparation;
	private MyTextField widthSeparation;
	private MyTextField totalWeight;
	private MyTextField totalWidth;
	private MyTextField height;
	
	/**
	 * Constructor that sets up the GUI and saves the pointers passed in the parameters
	 * as the object variables for future reference.
	 * 
	 * @param guru The Guru class that created this class.
	 * @param trip The Trip in the Guru class that created this class.
	 */
	public TripSetup(Guru guru, Trip trip){
		super(); // Call to the constructor of the super class.
		
		// Saving the pointers passed in the parameters as the object variables
		this.guru = guru;
		this.trip = trip;
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		
		this.add(new MyLabel("<html><h1>Transport Information</h1></html>",20,20,500,30));
		this.add(new MyLabel("Company Name:",20,70,150,20));
		this.add(new MyLabel("Date of Transport: ",20,95,500,20));
		this.add(new MyLabel("<html><h1>Vehicle Information</h1></html>",20,130,500,30));
		this.add(new MyLabel("Weight Separation: ",20,180,150,20));
		this.add(new MyLabel("Axle Separation: ",20,205,150,20));
		this.add(new MyLabel("Total Weight: ",20,230,150,20));
		this.add(new MyLabel("Total Length: ",20,255,150,20));
		this.add(new MyLabel("Height: ", 20, 280, 150, 20));
		
		this.companyName = new MyTextField(180,70,150,20);
		this.add(this.companyName);
		
		this.dateOfTransport = new MyTextField(180,95,150,20);
		this.add(this.dateOfTransport);
		
		this.weightSeparation = new MyTextField(180,180,150,20);
		this.add(this.weightSeparation);
		this.add(new MyLabel("t Example: 4 + 3 + 2 x 8 + 9", 340, 180, 200, 20));
		
		this.widthSeparation = new MyTextField(180,205,150,20);
		this.add(this.widthSeparation);
		this.add(new MyLabel("m Example: 4 + 3 + 2 x 8 + 9", 340, 205, 200, 20));
		
		this.totalWeight = new MyTextField(180,230,150,20);
		this.add(this.totalWeight);
		this.add(new MyLabel("t", 340, 230, 25, 20));
		
		this.totalWidth = new MyTextField(180,255,150,20);
		this.add(this.totalWidth);
		this.add(new MyLabel("m", 340, 255, 25, 20));
		
		this.height = new MyTextField(180, 280, 150, 20);
		this.add(this.height);
		this.add(new MyLabel("m", 340, 280, 25, 20));
		
		this.backButton = new MyButton("Cancel", 20,320,150,20,this);
		this.add(this.backButton);
		
		this.next = new MyButton("Next", 180,320,150,20,this);
		this.add(this.next);
	}

	/**
	 * The Action Listener for the Components on this BasePanel
	 */
	public void actionPerformed(ActionEvent e) {
		
		// Button BACK was pressed
		if(e.getSource() == this.backButton){
			/*
			 * Exiting the Trip Setup mode erases all information in the Trip of Guru and in
			 * the fields of screens that are used to set the Trip up.
			 */
			
			// Confirm the exiting.
			if(Tools.confirm("If you cancel the setup now, all your entered information will be lost.\nAre you sure you want to exit?",this)){
				//Exit the screen with the appropriate Guru method that erases all information.
				this.guru.exitTripSetupMode();
			}
		}
		
		// Button NEXT was pressed.
		if(e.getSource() == this.next){
			this.next(); // Switch the screen and save the information provided by the user in the fields.
		}
	}

	
	/**
	 * Saves all information provided by the user in the fields on the screen into the Trip og Guru.
	 * Also validates the information. 
	 */
	public void next(){
		try{
			/*
			 *  ASSESSMENT OF WEIGHT INFORMATION
			 */
			
			// Parse the provided information about the TOTAL WEIGHT OF THE VEHICLE from the text field into a double.
			double totalWeight = Double.parseDouble(this.totalWeight.getText().trim());
			
			// Get the values from the WEIGHT SEPARATION field in the form of an array with the values already extrapolated.
			double[] weight = Tools.toField(this.weightSeparation.getText().trim(), totalWeight);
			
			// Check if the provided WEIGHT SEPARATION matches the provided TOTAL WEIGHT (Redundancy check)
			if(weight == null){
				// If the weights do not match, inform the user of this error and return without saving anything.
				Tools.error("The values entered for the weight separation and total weight do not match.",this);
				return;
			}
			
			
			/*
			 *  ASSESSMENT OF AXLE SEPARATION INFORMATION
			 */
			
			// Parse the provided information about the TOTAL SEPARATION (width) OF THE VEHICLE from the text field into a double.
			double totalSeparation = Double.parseDouble(this.totalWidth.getText().trim());
			
			// Get the values from the AXLE SEPARATION field in the form of an array with the values already extrapolated.
			double[] separation = Tools.toField(this.widthSeparation.getText().trim(), totalSeparation);
			
			// Check if the provided AXLE SEPARATION matches the provided TOTAL SEPARATION (Redundancy check)
			if(separation == null){
				// If the separations do not match, inform the user of this error and return without saving anything.
				Tools.error("The values entered for the axle separation and total length do not match.",this);
				return;
			}
			
			/*
			 * ASSESSMENT AND SAVING OF THE PROVIDED INFORMATION 
			 */
			
			/*
			 * Check whether the amount of axle separations is one record shorter than the amount of weights provided
			 * by the user. The separations are BETWEEN the axles and the weights ARE the axles. Therefore there must
			 * only be as many axle separation records as there are spaces between axles.
			 */
			if(weight.length == (separation.length + 1)){
				
				// Get the double value of the height of the vehicle
				double height = Double.parseDouble(this.height.getText().trim());
				
				// Setup the Trip in Guru (call the setup method and pass all information in the parameters).
				this.trip.setup(weight, separation, totalWeight, totalSeparation, height, this.dateOfTransport.getText().trim(), this.companyName.getText().trim());
				
				// Check whether any information was provided as the name of the company and date of the transport.
				if(this.companyName.getText().equals("") || this.dateOfTransport.getText().equals("")){
					/* If some information is missing, inform the user of this, but enable the user to
					 * continue without the information.
					 */
					if(Tools.confirm("You did not fill out all the information in the top section\nProceed anyway?", this)){
						
						// Continue to the next screen.
						this.guru.setScreen(Screens.ROUTE_PLANNER);
					}
				}else{
					// Continue to the next screen.
					this.guru.setScreen(Screens.ROUTE_PLANNER);
				}
			}else{
				/* If the amount of weights and separations of axles to not match, inform the user of
				 * this error. (do not proceed to the next screen)
				 */
				Tools.error("The values provided for weight separation and width separation do not match.\nThe widths must corespond to the amount of axles.",this);
			}
		}catch(NumberFormatException ex){
			/* If any of the provided values that should be numeric are not numeric, inform the user of
			 * this error. (do not proceed to the next screen)
			 */
			Tools.error("Invalid values placed into the fields. Please write only numberic values in the bottom section",this);
		}catch(Exception ex){
			// If anything else goes wrong, print the Exception in an error message.
			Tools.error(ex.toString(),this);
		}
	}
	
	/**
	 * Empties all the fields on the screen.
	 */
	public void nullify() {
		// Set the text on all MyTextFields to nothing.
		this.companyName.setText("");
		this.dateOfTransport.setText("");
		this.height.setText("");
		this.totalWeight.setText("");
		this.totalWidth.setText("");
		this.weightSeparation.setText("");
		this.widthSeparation.setText("");
	}
}

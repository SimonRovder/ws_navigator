package dossier;

/**
 * The class used for storing the limitations posed by an Obstacle or Branch.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class Restriction {

	// The length of the Restriction.
	private double separation;
	
	// The height of the Restriction.
	private double height;
	
	// The three weight limits of the Restriction.
	private double[] weight;
	
	// The activity of the weights limitation.
	private boolean useWeights;
	
	// The activity of the height limitation.
	private boolean useHeights;
	
	// The ID of the Restriction.
	private String id;
	
	/**
	 * Constructor - Creates the empty object variables.
	 */
	public Restriction(){
		// Create the empty object variables.
		this.weight = new double[3];
		this.weight[0] = -2;
		this.weight[1] = -2;
		this.weight[2] = -2;
		this.separation = -2;
		this.height = -2;
		this.id = "";
		this.useHeights = false;
		this.useWeights = false;
	}
	
	/**
	 * Constructor - Creates the class and fills the object variables with the values passed in the
	 * parameters.
	 * 
	 * @param id The ID of the Restriction
	 * @param separation The length of the Restriction
	 * @param height The height limit of the Restriction
	 * @param w1 The NORMAL weight limit of the Restriction.
	 * @param w2 The EXCLUSIVE weight limit of the Restriction.
	 * @param w3 The EXTRAORDINARY weight limit of the Restriction.
	 * @param weights The activity of the weight limits.
	 * @param heights The activity of the height limits.
	 */
	public Restriction(String id, double separation, double height, double w1, double w2, double w3, boolean weights, boolean heights){
		// Create the array of doubles.
		this.weight = new double[3];
		
		// set up the object variables.
		this.setup(id, separation, height, w1, w2, w3, weights, heights);
	}
	
	/**
	 * Fills the object variables with the values passed in the
	 * parameters.
	 * 
	 * @param id The ID of the Restriction
	 * @param separation The length of the Restriction
	 * @param height The height limit of the Restriction
	 * @param w1 The NORMAL weight limit of the Restriction.
	 * @param w2 The EXCLUSIVE weight limit of the Restriction.
	 * @param w3 The EXTRAORDINARY weight limit of the Restriction.
	 * @param weights The activity of the weight limits.
	 * @param heights The activity of the height limits.
	 */
	public void setup(String id, double separation, double height, double w1, double w2, double w3, boolean weights, boolean heights){
		// Save the values provided in the parameters as the object variables.
		this.id = id;
		this.separation = separation;
		this.height = height;
		this.weight[0] = w1;
		this.weight[1] = w2;
		this.weight[2] = w3;
		this.useWeights = weights;
		this.useHeights = heights;
	}
	
	/**
	 * Returns the String containing the activity of the limitations represented as two ones and zeros.
	 * @return The String containing the activity of the limitations represented as two ones and zeros.
	 */
	public String getActivity(){
		// Prepare an empty String.
		String s = "";
		
		// Check and write the activity of the weight limits.
		if(this.useWeights) s = s + "1"; else s = s + "0";
		
		// Check and write the activity of the height limits.
		if(this.useHeights) s = s + "1"; else s = s + "0";
		
		// Return the String.
		return s;
	}
	
/*
 * MASTERY FACTOR 8 - Encapsulation.
 */	
	
	/**
	 * Formats the limitations of the Restriction into HTML format and returns the finished String.
	 * @return The limitations of the Restriction in HTML format.
	 */
	public String toHTMLString(){
		// Return the finished String.
		return ("<html>Id: " + this.id + "<br>Separation: " + Tools.isUnknown(this.separation) + "<br>Height: " + Tools.isUnknown(this.height) + "<br>Normal: " + Tools.isUnknown(this.weight[0]) + "<br>Exclusive: " + Tools.isUnknown(this.weight[1]) + "<br>Etraodinary: " + Tools.isUnknown(this.weight[2]) + "<html>");
	}

	public String getText(){
		return (this.height + this.id + this.separation + this.weight[0] + this.weight[1] + this.weight[2] + "000");
	}
	
	/**
	 * Getter for the object variable storing the length of the currently edited Restriction.
	 * @return The object variable storing the length of the currently edited Restriction.
	 */
	public double getSeparation() {
		return separation;
	}
	
	/**
	 * Setter for the object variable storing the length of the currently edited Restriction.
	 * @param separation The new length of the currently edited Restriction.
	 */
	public void setSeparation(double separation) {
		this.separation = separation;
	}
	
	/**
	 * Getter for the object variable storing the height limit of the currently edited Restriction.
	 * @return The object variable storing the height limit of the currently edited Restriction.
	 */
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	/**
	 * Getter for one of the three weight limit levels.
	 * @param level The weight limit level.
	 * @return The weight limit.
	 */
	public double getWeight(int level) {
		return weight[level];
	}

	/**
	 * Setter for one of the three weight limit levels.
	 * @param level The weight limit level.
	 * @param weight The new weight limit for that level.
	 */
	public void setWeight(int level, double weight) {
		this.weight[level] = weight;
	}

	/**
	 * Getter for the ID of this Restriction.
	 * @return The ID of this Restriction.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Setter for the ID of this Restriction.
	 * @param id The ID of this Restriction.
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * The getter for the activity of the weight limits of this Restriction.
	 * @return The activity of the weight limits of this Restriction.
	 */
	public boolean isUseWeights() {
		return useWeights;
	}

	/**
	 * The setter for the activity of the weight limits of this Restriction.
	 * @param useWeights The activity of the weight limits of this Restriction.
	 */
	public void setUseWeights(boolean useWeights) {
		this.useWeights = useWeights;
	}

	/**
	 * Getter for the activity of the height limit of this Restriction.
	 * @return The activity of the height limit of this Restriction.
	 */
	public boolean isUseHeights() {
		return useHeights;
	}

	/**
	 * Setter for the activity of the height limit of this Restriction.
	 * @param useHeights The activity of the height limit of this Restriction.
	 */
	public void setUseHeights(boolean useHeights) {
		this.useHeights = useHeights;
	}
}

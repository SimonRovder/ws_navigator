package dossier;
/**
 * Class that stores Strings of all the different modes:
 * NORMAL
 * EXCLUSIVE
 * EXTRAORDINARY
 * UNKNOWN
 * CRASH
 * 
 * All are public, so that they can be edited with ease.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class Report {
	
	// The Strings for all the different modes.
	public String normal;
	public String exclusive;
	public String extraordinary;
	public String unknown;
	public String crash;
	
	/**
	 * Constructor makes sure the object variables (in this case all the Strings) are empty.
	 */
	public Report(){
		this.normal = "";
		this.extraordinary = "";
		this.exclusive = "";
		this.crash = "";
		this.unknown = "";
	}
	

	/**
	 * Returns all the Strings in the Report, added together in a single String.
	 * @return All the Strings in the Report, added together in a single String
	 */
	public String toString(){
		// Add up all the Strings in the object variables.
		String s = "NORMAL : \n" + this.normal;
		s = s + "\n\nEXCLUSIVE : \n" + this.exclusive;
		s = s + "\n\nEXTRAORDINARY : \n" + this.extraordinary;
		s = s + "\n\nUNKNOWN : \n" + this.unknown;
		s = s + "\n\nCRASH : " + this.crash;
		// Return the summed up Strings.
		return s;
	}
}
package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;

/**
 * Takes care of the GUI for backup saving and backup loading.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class Management extends BasePanel implements ActionListener{
	private static final long serialVersionUID = 5847652801292694846L;

	// Pointer to the Guru class that created this class.
	private Guru guru;
	
	// The JFileChooser used to enable the user to select the path to the backup.
	private JFileChooser chooser;
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton load;
	private MyButton save;
	private MyLabel label;
	private MyButton backButton;
	
	/**
	 * Constructor saves the pointer passed in the parameters to the
	 * object variable and creates the GUI along with other necessary classes.
	 * 
	 * @param guru The Guru class that created this class.
	 */
	public Management(Guru guru){
		super(); // Call to the constructor of the superclass.
		
		// Save the passed Guru as the object variable.
		this.guru = guru;
		
		// Create and set up the JFileChooser.
		this.chooser = new JFileChooser();
		this.chooser.setMultiSelectionEnabled(false);
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.load = new MyButton("Load Backup",150,50,220,40,this);
		this.add(this.load);
		this.save = new MyButton("Save Backup",410,50,220,40,this);
		this.add(this.save);
		this.backButton = new MyButton("Back",280,120,220,30,this);
		this.add(this.backButton);
		this.label = new MyLabel("", 20, 120, 300, 30);
		this.add(this.label);
	}

	/**
	 * The action listener
	 */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.load){ // The user clicked the LOAD BACKUP button.
			// Enable the user to browse for the backup file.
			if(this.chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
				// At this point, the user selected a path to the backup file and clicked SAVE.
				
				try {
					/*
					 * The program saves an emergency backup file at this point just in case.
					 */
					this.guru.fileManager.saveBackupFile(Tools.backupFileName, this.guru.highwaySystem);
				}catch(Exception ex){
					/*
					 * If the emergency backup file cannot be saved, inform the user of this error and do
					 * not continue. (return)
					 */
					Tools.error("An internal error occured while trying to save an emergency backup file.\nPlease contact me at " + Tools.address + " for further assistance.\n\n" + ex.toString(), this);
					return;
				}
				/*
				 * At this point, the emergency backup file is saved and the loading can commence.
				 */
				try{
					/*
					 * Create a new HighwaySystem and load it passing the path the user specified to the
					 * FileManager class. 
					 */
					HighwaySystem hs = this.guru.fileManager.loadBackup(this.chooser.getSelectedFile().getAbsolutePath());
					// Set the pointer in Guru to the new loaded HighwaySystem.
					this.guru.highwaySystem = hs;
					// Create the panels of both the Editor and the RoutePlanner to show the new HighwaySystem.
					this.guru.editor.createPanels();
					this.guru.routePlanner.createPanels();
					
					// Inform the user that the backup was loaded.
					Tools.message("Backup Loaded.", this);
				} catch (Exception ex) {
					// If the backup file was wrong and could not be loaded, inform the user of this error.
					Tools.error("An error occured while reading the backupFile.\nThe backup file is wrong.", this);
					try{
						// Load the emergency backup file saved before loading.
						this.guru.fileManager.loadBackup(Tools.backupFileName);
					}catch(Exception exx){
						// If the loading of the emergency backup fails, inform the user of this error.
						Tools.error("An internal error occured while trying to read an emergency backup file.\nPlease contact me at " + Tools.address + " for further assistance.\n\nThe program will now undergo a factory reset.", this);
						// Initiate a factory reset to save the program.
						this.guru.factoryReset();
						return;
					}
				}
			}
		}
		if(e.getSource() == this.save){ // The user clicked the SAVE BACKUP button.
			// Enable the user to browse for the backup file.
			if(this.chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION){
				// At this point, the user selected a path and clicked SAVE.
				try {
					// Save the backup to the provided file path via the FileManager.
					this.guru.fileManager.saveBackupFile(this.chooser.getSelectedFile().getAbsolutePath() + ".BACKUP", this.guru.highwaySystem);
					// Inform the user that the backup was saved.
					Tools.message("Backup saved.", this);
				} catch (Exception ex) {
					Tools.error("An error occured while saving the backup file. The backup file was not saved.\n\n" + ex.getMessage(),this);
				}
			}
		}
		
		// If the back button is pressed, set the screen to the main menu.
		if(e.getSource() == this.backButton){
			this.guru.setScreen(Screens.MAIN_MENU);
		}
	}
}
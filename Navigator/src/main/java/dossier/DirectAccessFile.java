package dossier;

import java.io.*;

/**
 * Class manages all reading  of and writing into random access files.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
/*
 * MASTERY FACTOR 11 -- 13 - File input output.
 */
public class DirectAccessFile{
	
	// The RandomAccessFile that this class will work with.
    private RandomAccessFile file;
    
    // The length of a single record in the opened random access file.
    private long recordLength;
    
    /**
     * Simple constructor - creates the class.
     */
    public DirectAccessFile(){}
    
    /**
     * Opens the random access file, the directory to which is supplied in the parameters.
     * Saves the length of a single record in the object variable.
     * 
     * @param fileName The directory of the file that is to be opened.
     * @param recordLength The length of a single record of the file that is to be opened. 
     * @throws Exception
     */
    public void openFile(String fileName, long recordLength) throws Exception{
    	// Save the length of a single record in the object variable.
    	this.recordLength = recordLength;
    	
    	// Open the random access file.
        this.file = new RandomAccessFile(Tools.resourceLink + fileName, "rw");
    }
    
    /**
     * Returns the number of records in the opened file.
     * 
     * @return The number of records in the opened file.
     * @throws Exception
     */
    public long getRecordNumber() throws Exception{
    	/*
    	 * Divide the total length of the file by the length of a single records to obtain the
    	 * total amount of records in the file. Return this amount.
    	 */
        return (this.file.length() / this.recordLength);
    }
    
    /**
     * Read the record from the position passed in the parameters in the opened file.
     * 
     * @param position The index of the record that is to be read.
     * @return The record at the provided position.
     * @throws Exception
     */
    public String read(long position) throws Exception{
    	// Find the beginning of the requested record.
        this.file.seek(position*this.recordLength);
        
        // Read the record.
        String s = this.file.readUTF();
        
        /*
         * All the records in the random access file are of the same length. If a record
         * is not of this length before writing, spaces are added to make it long enough. Thus the record
         * needs to be trimmed first, to get rid of the additional spaces.
         */
        s = s.trim();
        
        // Return the trimmed record.
        return s;
    }
    
    /**
     * Write a single record to the provided position, rewriting any record that is there already.
     * 
     * @param text A String containing the text that is to be written to the given position.
     * @param position The position to which the given text is to be written.
     * @throws Exception
     */
/*
 * MASTERY FACTOR 1 - Adding data to an instance of the RandomAccessFile by direct manipulation of the file pointer
 * using the seek method.
 */
    public void writeTo(String text, long position) throws Exception{
    	// Check whether the provided String containing the text is not null.
        if(text != null){
        	// If it is not null, continue.
        	
        	// Find the position to which the new record is to be written.
	        this.file.seek(position * this.recordLength);
	        
	        
	        // Prepare the new record for writing to the file (correct its length).
	        text = this.prepareForWriting(text);
	        
	        // Write the record (now of the correct length) into the file.
            this.file.writeUTF(text);                
        }else{
        	// If the provided String is null, throw an Exception. 
        	throw new Exception("A null String was passed to DirectAccessFile.writeTo()");
        }
    }
    
    /**
     * Write a single record to the provided position, rewriting any record that is there already.
     * 
     * @param text A String containing the text that is to be written to the given position.
     * @param position The position to which the given text is to be written.
     * @throws Exception
     */
    public void insert(String text, long position) throws Exception{
    	// Check whether the provided String containing the text is not null.
        if(text != null){
        	// If it is not null, continue.
        	
        	// Get the position one higher than that of the very last record.
	        long looper = this.getRecordNumber();
	        
	        // Iterate through all the records, moving them upwards, until the required position is reached.
	        while(looper > position){
	        	/*
	        	 * Copy the record one position bellow the current position to the current position.
	        	 * This creates a duplicate of the last record one position above it.
	        	 */
	            this.writeTo(this.read(looper - 1), looper);
	            
	            // continue iteration by the next lower record.
	            looper--;
	        }
	        
	        /*
	         * Once the specified position is reached and the record at it is duplicated on the position one
	         * record further, write the new record to the position, which is now free to be changed without
	         * any loss of records. All records above it were shifted one record higher.
	         */
	        this.writeTo(text, position);
	    }else{
        	// If the provided String is null, throw an Exception. 
	    	throw new Exception("A null String was passed to DirectAccessFile.writeTo()");
	    }
    }
    
    /**
     * Deletes the record at the provided position in the opened random access file.
     * 
     * @param position The position of the record that is to be deleted.
     * @throws Exception
     */
/*
 * MASTERY FACTOR 2 - Deleting data from an instance of the RandomAccessFile by direct manipulation of the file pointer
 * using the seek method.
 */
    public void delete(long position) throws Exception{
    	// Get the position of the very last record.
    	long looper = this.getRecordNumber()-1;
    	
    	// Iterate, starting at the position of the record that is to be deleted.
    	while(position < looper){
    		// Overwrite the current position with the record one position higher than it.
    		this.writeTo(this.read(position+1), position);
    		// Continue iteration at the next position.
    		position++;
    	}
    	
    	/*
    	 * Once this is finished, all records above the record that was to be deleted, were shifted down,
    	 * rewriting the record that was to be deleted. At the end of the file, remains the same last record
    	 * at the two last positions. So the file is shortened by one position, to exclude one of the records.
    	 */
    	this.file.setLength(this.file.length() - this.recordLength);
    }
    
    /**
     * Appends the record provided in the String to the very end of the opened random access file.
     * 
     * @param text The String containing the record that is to be appended.
     * @throws Exception
     */
    public void append(String text) throws Exception{
    	// Insert the provided record to the position above the very last record (append it to the end).
        insert(text, this.getRecordNumber());
    }
    
    /**
     * Prepare the provided String for writing into the random access file, by making it as long as
     * the record length specifies.
     * 
     * @param text The record that is to be added to the file.
     * @return The record that is to be added to the file, now of correct length
     */
    
    private String prepareForWriting(String text) throws Exception{
    	
    	// Get the length of the provided String in bytes of UTF8 encoding.
        long textLength = Tools.getUTFLength(text) + 2;
        
        // Calculate the amount of necessary spaces by subtracting the length from the required record length.
        long spaces = this.recordLength - textLength;
        
        // Check the amount of spaces that needs to be added.
        if(spaces < 0){
        	// If the amount of spaces is negative, the record is too long. Hence throw an Exception.
            throw new Exception("Too long a record was passed to the DirectAccessFile.prepareForWriting().");
        }else{
        	/*
        	 * If the amount of spaces is not negative, add the required amount of spaces to the end
        	 * of the record by iteration.
        	 */
            while(spaces > 0){
            	// Add a space as long as the amount of spaces is not 0.
                text = text + " ";
                // Continue iteration with the next space.
                spaces--;
            }
        }
        // Return the record, now of correct length.
        return text;
    }
    
    /**
     * Resets the file - makes it empty.
     * @throws Exception
     */
    public void resetFile() throws Exception{
    	// Set the length of the opened file to zero (exclude all content).
		this.file.setLength(0);
    }
    
    /**
     * Closes the opened random access file.
     * @throws IOException
     */
    public void close() throws IOException{
    	// Close the file.
    	this.file.close();
    }
}
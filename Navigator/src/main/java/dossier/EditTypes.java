package dossier;

/**
 * The enum class containing the enumerated values of all possible
 * editing actions the user may have taken in the Editor.
 * 
 * (class used to specify how the program should handle an edit)
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public enum EditTypes {
	EDIT_BRANCH,
	EDIT_OBSTACLE,
	ADD_OBSTACLE_TO,
	ADD_BRANCH_TO
}
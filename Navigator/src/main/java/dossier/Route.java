package dossier;


/**
 * Contains all the information of a single Route taken on a certain highway.
 * Extends Node, as it will be a part of a LinkedList of Routes (the Trip)
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class Route extends Node{

	// The first Obstacle the vehicle will encounter on the Highway
	private Obstacle start;
	
	// The last Obstacle the vehicle will encounter on the Highway.
   	private Obstacle end;
   	
   	// Boolean determining whether the vehicle will be entering the Highway via a Branch.
   	private boolean useEntryBranch;
   	// The pointer to the Branch via which the vehicle will enter the Highway.
   	private Branch entry;
   	
   	// Boolean determining whether the vehicle will be exiting the Highway via a Branch.
   	private boolean useExitBranch;
   	// The pointer to the Branch via which the vehicle will enter the Highway.
   	private Branch exit;
   	
   	// The Highway the vehicle will be traveling on. 
   	private Highway highway;
   	
   	/* Boolean determining the direction of travel.
   	 * true -> up the Highway
   	 * false -> down the Highway
   	 * This is necessary when the vehicle will encounter only one Obstacle and the program therefore 
   	 * cannot determine the direction on its own.
   	 */
   	private boolean direction;
   
   	/**
   	 * Constructor saves all passed parameters as object variables for future reference.
   	 * @param highway The Highway which this Route uses.
   	 * @param start The first Obstacle the vehicle will encounter on the Highway
   	 * @param end The last Obstacle the vehicle will encounter on the Highway.
   	 * @param entry The pointer to the Branch via which the vehicle will enter the Highway.
   	 * @param exit The pointer to the Branch via which the vehicle will enter the Highway.
   	 * @param useEntryBranch Boolean determining whether the vehicle will be entering the Highway via a Branch.
   	 * @param useExitBranch Boolean determining whether the vehicle will be exiting the Highway via a Branch.
   	 * @param direction Boolean determining the direction of travel.
   	 */
  	public Route(Highway highway, Obstacle start, Obstacle end, Branch entry, Branch exit, boolean useEntryBranch, boolean useExitBranch, boolean direction){
  		// Save all passed information as object variables.
	  	this.entry = entry;
	  	this.exit = exit;
		this.end = end;
		this.start = start;
		this.useEntryBranch = useEntryBranch;
		this.useExitBranch = useExitBranch;
		this.highway = highway;
		this.direction = direction;
   	}
	
  	/**
  	 * Getter for the object variable storing the first encountered Obstacle on the Route.
  	 * @return The object variable storing the first encountered Obstacle on the Route.
  	 */
	public Obstacle getStart() {
		return start;
	}

	/**
	 * Getter for the object variable storing the last encountered Obstacle on the Route.
	 * @return The object variable storing the last encountered Obstacle on the Route.
	 */
	public Obstacle getEnd() {
		return end;
	}

	/**
	 * Getter for the object variable saying whether an entry Branch is used on the Route.
	 * @return The object variable saying whether an entry Branch is used on the Route.
	 */
	public boolean isUseEntryBranch() {
		return useEntryBranch;
	}

	/**
	 * Getter for the object variable saying whether an exit Branch is used on the Route.
	 * @return The object variable saying whether an exit Branch is used on the Route.
	 */
	public boolean isUseExitBranch() {
		return useExitBranch;
	}

	/**
	 * Getter for the object variable storing the Entry Branch used on the Route.
	 * @return The object variable storing the Entry Branch used on the Route.
	 */
	public Branch getEntry() {
		return entry;
	}

	/**
	 * Getter for the object variable storing the Exit Branch used on the Route.
	 * @return The object variable storing the Exit Branch used on the Route.
	 */
	public Branch getExit() {
		return exit;
	}

	/**
	 * Getter for the object variable storing the Highway used on the Route.
	 * @return The object variable storing the Highway used on the Route.
	 */
	public Highway getHighway() {
		return highway;
	}
	
	/**
	 * Getter for the object variable saying the direction of travel on the Route.
	 * @return The object variable saying the direction of travel on the Route.
	 */
	public boolean getDirection(){
		return this.direction;
	}
}

package dossier;

/**
 * This is a class that all Classes that are to be used as items in a LinkedList will extend.
 * It provides all the functions of a LinkedList item.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */

public class Node {
	protected Node next; 		// Next Node in the LinkedList
	protected Node previous;	// Previous Node in the LinkedList
	
	/**
	 * Constructor (sets object variables to null)
	 */
	public Node(){
		this.next = null;
		this.previous = null;
	}
	
	/**
	 * Sets the pointer to the next Node in the LinkedList.
	 * @param next The next Node.
	 */
	public void setNext(Node next){
		this.next = next;
	}
	
	/**
	 * Sets the pointer to the previous Node of the LinkedList.
	 * @param previous The previous Node.
	 */
	public void setPrevious(Node previous){
		this.previous = previous;
	}
	
	/**
	 * Returns the pointer to the next Node in the LinkedList.
	 * @return The next Node.
	 */
	public Node getNext(){
		return this.next;
	}
	
	/**
	 * Returns the pointer to previous Node in the LinkedList 
	 * @return The previous Node
	 */
	public Node getPrevious(){
		return this.previous;
	}
}

package dossier;

/**
 * This class stores all information about a branch.
 * It extends the BasicOB class and hence also extends
 * the Node and can be used as an item in a LinkedList.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class Branch extends BasicOB{
	
	// Variable that contains the restriction posed by this Branch.
	private Restriction restriction;
	
	/**
	 * Constructor creates the object and sets the object variables to the pointers passed in the parameters.
	 * 
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 * @param restriction The restriction posed by this Branch.
	 */
	public Branch(int index, String type, double km, String description, Restriction restriction){
		// Call the constructor of the superclass and pass the required parameters to it.
		super(index, type, km, description);
		
		// Set the object variable Restriction to the Restriction provided in the parameters.
		this. restriction = restriction;
	}
	
	/**
	 * Saved the main information about the transport taking place.
     * (Saves the parameters as the corresponding object variables)
     * 
	 * @param index The index of this Branch or Obstacle in its Random Access File.
	 * @param type String that contains either "VETVA", "MOST" or "NAD", depending on the type.
	 * @param km Location on the Highway (distance from the start of the Highway)
	 * @param description String that contains the user-defined description of the Branch or Obstacle
	 * @param restriction The restriction posed by this Branch.
	 */
	public void setup(int index, String type, double km, String description, Restriction restriction){
		super.setup(index, type, km, description);
		this.restriction = restriction;
	}
	
	/**
	 * Getter for the object variable storing the Restriction posed by this Branch.
	 * @return The Restriction.
	 */
	public Restriction getRestriction() {
		return this.restriction;
	}
}
package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * This class manages the saving of Routes to the Trip in Guru. It manages
 * the RoutePlannerPanels that take care of creation of Routes on
 * single Highways, and they then pass the Routes to this class.
 * 
 * This class also manages the already existing Routes and a JList that
 * displays them.
 * 
 * This class extends the BasePanel and is one of the screens that can
 * be displayed in the Guru MyFrame.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */

public class RoutePlanner extends BasePanel implements ActionListener{
	private static final long serialVersionUID = -4061517438059849320L;

	/* Pointer to the Guru class that created this class. (Used to call
	 * methods of Guru).
	 */
	private Guru guru;
	
	/*
	 * The Trip that is created in Guru. The Trip extends the LinkedList,
	 * so this class can add Routes to the Trip.
	 */
	private Trip trip;
	
	
	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private JTabbedPane tabbedPane;
	
	private MyButton delete;
	private MyButton process;
	private MyButton back;
	
	private JScrollPane routeScroller;
	private JList routes;
	
	
	// Array of RoutePlannerPanels (they will be placed in the JTabbedPane)
	private RoutePlannerPanel[] routePlannerPanels;
	
	/**
	 * Constructor saves the pointers passed in the parameters to the
	 * object variables and creates the GUI.
	 * 
	 * @param guru The Guru class that created this class.
	 * @param trip The Trip in the Guru class that created this class.
	 */
	public RoutePlanner(Guru guru, Trip trip){
		super(); // Call to the constructor of the superclass.
		
		// Save the pointers passed in the parameters as the object variables.
		this.trip = trip;
		this.guru = guru;
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.tabbedPane = new JTabbedPane();
		this.tabbedPane.setBounds(20,20,500,520);
		this.tabbedPane.setVisible(true);
		this.add(this.tabbedPane);
		
		this.process = new MyButton("Process", 560, 480, 180, 60, this);
		this.add(this.process);
		
		this.delete = new MyButton("Delete", 560, 250, 180, 30, this);
		this.add(this.delete);
		
		this.back = new MyButton("Back", 560, 300, 180, 30, this);
		this.add(this.back);
		
		this.routes= new JList();
		this.routeScroller = new JScrollPane(this.routes);
		this.routeScroller.setBounds(540,40,220,200);
		this.routeScroller.setVisible(true);
		this.add(this.routeScroller);
	}
	
	/**
	 * Creates a routePlannerPanel for each existing Highway in the HighwaySystem of Guru.
	 * Places these new panels onto the JTabbedPane.
	 */
	public void createPanels(){
		this.tabbedPane.removeAll(); // Remove everything from the JTabbedPane.
		
		// Create an array of as many RoutePlannerPanels as there are Highways.
		this.routePlannerPanels = new RoutePlannerPanel[this.guru.highwaySystem.getHighways()];
		
		// Start at the first Highway.
		Highway temp = (Highway) this.guru.highwaySystem.getFirst();
		int loop = 0;
		// Iterate through all the Highways.
		while(temp != null){
			// Create a RoutePnallerPanel for the current Highway.
			this.routePlannerPanels[loop] = new RoutePlannerPanel(temp, this);
			
			// Add the new RoutePlannerPanel to the JTabbedPane.
			this.tabbedPane.add(this.routePlannerPanels[loop], temp.getName());
			
			// Continue iteration with the next Highway.
			temp = (Highway) temp.getNext();
			loop++;
		}
		this.updatePanels(); // Update the newly created RoutePlannerPanels
	}
	
	/**
	 * Updates all RoutePlannerPanels in the JTabbedPane.
	 */
	public void updatePanels(){
		/*
		 * Iterate through all the RoutePlannerPanels in the array and call their
		 * updatePanel method to update them.
		 */
		for(int i = 0; i < this.routePlannerPanels.length; i++){
			this.routePlannerPanels[i].updatePanel();
		}
	}
	
	/**
	 * Add the Route passed in the parameters to the Trip in Guru.
	 * (the Trip extends a LinkedList)
	 * 
	 * @param route The new Route.
	 */
	public void addRoute(Route route){
		try{
			this.trip.addRoute(route);	// Add the Route to the LinkedList of Routes.
			this.refreshRoutes();		// Refresh the JList showing all the Routes.
		}catch(Exception ex){
			// If the passed Route is invalid, inform the user of this error.
			Tools.error(ex.toString() + "Invalid information entered.",this);
		}
	}
	
	/**
	 * This method refreshes the JList showing all the Routes in the Trip.
	 * (all the Routes that will be assessed)
	 */
	public void refreshRoutes(){
		// Create an array of as many Objects as there are Routes in the Trip.
		Object[] o = new Object[this.trip.getRoutes()];
		
		// Start at the first Route
		Route temp = (Route) this.trip.getFirst();

		// Iterate through all the Routes.
		for(int i = 0; temp != null; i++){
			// Add the current Route (correctly formated) into the array.
			o[i] = temp.getHighway().getName() + " -- From:" + temp.getStart().getRight().getId() + " To:" + temp.getEnd().getLeft().getId();
			// Continue iteration with the next Route.
			temp = (Route) temp.getNext();
		}
		// Set the array as the content of the JList showing the Routes.
		this.routes.setListData(o);
		
		// Refresh the screen to display the changed JList.
		this.validate();
	}
	
	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		
		// If the user pressed the BACK button, switch to the previous screen.
		if(e.getSource() == this.back){
			this.guru.setScreen(Screens.TRIP_SETUP);
		}
		
		// If the user pressed the DELETE button, proceed with Route deletion.
		if(e.getSource() == this.delete){
			try {
				// Delete the Route selected in the JList from the Trip in Guru.
				this.trip.delete(this.trip.getRoute(this.routes.getSelectedIndex()));
				
				// Refresh the JList to show the changes.
				this.refreshRoutes();
				
			} catch (Exception ex) {
				// If no Route was selected, inform the user of this error.
				Tools.error("Please select a valid route.",this);
			}
		}
		
		// If the user pressed the PROCESS button, initiate the processing procedure.
		if(e.getSource() == this.process){
			this.guru.process(); // Call the process method in Guru to assess the entire Trip.
		}
	}
	
	/**
	 * Empty all values the user may have selected in the RoutePlannerPanels.
	 */
	public void nullify() {
		// Iterate through all the routePlannerPanels, nullifying each one in the process.
		for(int i = 0; i < this.routePlannerPanels.length; i++){
			this.routePlannerPanels[i].nullify();
		}
	}
}
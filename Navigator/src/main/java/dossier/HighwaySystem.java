package dossier;

/**
 * The LinkedList containing all the Highways. (extends LinkedList)
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 * 
 */
/*
 * MASTERY FACTOR 10 - Implementing a hierarchical composite data structure.
 */
public class HighwaySystem extends LinkedList{
	
	/**
	 * Constructor creates the class.
	 */
	public HighwaySystem(){
		super(); // Call to the constructor of the superclass.
	}
	
	/**
	 * Checks whether there is no Highway with the same name as the name provided in the parameters. 
	 * @param name
	 * @return True if the name is free, false if the name is used.
	 */
	public boolean isNameFree(String name){
		Highway searcher = (Highway) this.first; // Get the first Highway in the LinkedList.
		while(searcher != null){
			/*
			 * Iterate through all the Highways, keep checking their names and return false if any of them
			 * match the provided name.
			 */
			if(searcher.getName().equals(name)){
				return false;
			}
			// Continue iteration with the next Highway.
			searcher = (Highway) searcher.getNext();
		}
		// If false was not returned by now, the name is not used and hence true can be returned.
		return true;
	}
	
	
	/**
	 * Getter for the amount of Highways in the HighwaySystem.
	 * @return The amount of Highways in the HighwaySystem.
	 */
	public int getHighways() {
		return this.amount;
	}

	/**
	 * Returns the Highway at the provided index in this LinkedList.
	 * @param index The index of the required Highway.
	 * @return The required Highway.
	 */
	public Highway getHighway(int index) {
		return (Highway) this.getNode(index);
	}

}

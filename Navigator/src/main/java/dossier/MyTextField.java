package dossier;

import javax.swing.JTextField;
/**
 * This class has the purpose of making JTextField creation easier. It extends a JTextField
 * and provides a more efficient constructor.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class MyTextField extends JTextField{
	private static final long serialVersionUID = 6141073554516821598L;

	/**
	 * Constructor that sets up the JTextField
	 * @param text Text to be written on the JTextField
	 * @param xPos X Position of the JTextField
	 * @param yPos Y Position of the JTextField
	 * @param width Width of the JTextField
	 * @param height Height of the JTextField
	 */
	public MyTextField(int xPos, int yPos, int width, int height){
		super(); // Call to the Constructor of the super class, passing the text for the JLabel.
		this.setBounds(xPos,yPos,width,height); // Setting the Bounds of the JLabel to those provided.
		this.setVisible(true);					// Making the JLabel visible.
	}
}
package dossier;

import javax.swing.JPanel;

/**
 * Class that extends the JPanel and that all GUI classes extend.
 * Created for possible future edits to the panels.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class BasePanel extends JPanel{
	private static final long serialVersionUID = -8149581661689923292L;

	/**
	 * Constructor - Creates the class
	 */
	public BasePanel(){
        super(); // Call to the constructor of the superclass.
        this.setLayout(null); // Set the layout to null.
    }
}
package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class that creates a window used for editing Branches and Obstacles (via PromptPanels).
 * It extends the MyFrame class.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 * 
 */

public class Prompt extends MyFrame implements ActionListener{
	private static final long serialVersionUID = -8993623250858158633L;

	/*
	 * A pointer to the Editor that will call toPrompt methods of this object. Used to call
	 * its methods once editing is complete.
	 */
	private Editor parent;
	
	// PromptPanels into which individual Restrictions of the edited Obstacle or Branch will be loaded.
	public PromptPanel single;
	public PromptPanel left;
	public PromptPanel right;
	
	// The pointer to the currently edited Obstacle.
	public Obstacle obstacle;
	
	// The pointer to the currently edited Obstacle.
	public Branch branch;
	

	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton exit;
	private MyButton save;
	public MyTextField description;
	public MyTextField km;
	public double kmDouble;
	
	/**
	 * Constructor creates the MyFrame and all the Components forming the GUI. Saves the pointer
	 * provided in the parameters as the corresponding object variable.
	 *  
	 * @param parent Pointer to the Editor that will call toPrompt methods of this object.
	 */
	public Prompt(Editor parent){
		//Call to the Constructor of the superclass (create the MyFrame window)
		super("Prompt");
		
		// Set the dimensions of the window.
		this.setSize(730, 500);
		// Place the window into the center of the screen.
		this.centerPosition();
		// Set the layout of the window to null.
		this.setLayout(null);
		
		// Save the pointer passed in the parameters as the corresponding object variable.
		this.parent = parent;
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the ContentPane.
		 */
		this.single = new PromptPanel("Branch");
		this.single.setBounds(175, 80, 340, 220);
		this.getContentPane().add(this.single);
		
		this.left = new PromptPanel("Left");
		this.left.setBounds(0, 80, 340, 220);
		this.getContentPane().add(this.left);
		
		this.right = new PromptPanel("Right");
		this.right.setBounds(360, 80, 340, 220);
		this.getContentPane().add(this.right);

		this.description = new MyTextField(150, 10, 500, 20);
		
		this.km = new MyTextField(150, 40, 500, 20);
		
		this.add(new MyLabel("Description:",30,10,100,20));
		this.add(new MyLabel("Kilometer:",30,40,100,20));
		
		this.add(this.description);
		this.add(this.km);
		
		
		this.add(new MyLabel("WARNING! Invalid characters (\";\", \"<\" and \">\") will be removed from the IDs and the description!",80,330,560,20));
		
		this.exit = new MyButton("Cancel",200,370,140,30,this);
		this.save = new MyButton("Save",360,370,140,30,this);
		this.getContentPane().add(this.save);
		this.getContentPane().add(this.exit);
		
		
		// Set the default close operation to DO NOTHING (user can exit only via CANCEL button).
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		// Hide the window.
		this.setVisible(false);
	}
	
	/**
	 * Sets only the correct PromptPanels as visible, depending on whether an Obstacle or Branch is being edited.
	 * @param screen Boolean describing the mode (true -> Obstacle) (false -> Branch).
	 */
	private void setScreen(boolean screen){
		// Set the correct visibility to the PromptPanels.
		this.right.setVisible(screen);
		this.left.setVisible(screen);
		this.single.setVisible(!screen);
	}
	
	/**
	 * Loads an Obstacle into the Prompt window for editing.
	 * @param obstacle The Obstacle that is to be edited.
	 * @throws Exception
	 */
/*
 * MASTERY FACTOR 6 - Polymorphism. 
 */
	public void toPrompt(Obstacle obstacle) throws Exception{
		// Check whether the Obstacle exists.
		if(obstacle != null){
			// Save the pointer to the Obstacle.
			this.obstacle = obstacle;
			
			// Set the correct PromptPanels as visible.
			this.setScreen(true);
			
			// Load the two Restrictions into the corresponding (and now visible) PromptPanels.
			this.left.setPanel(obstacle.getLeft());
			this.right.setPanel(obstacle.getRight());
			
			// Display the description and kilometer of the Obstacle in their corresponding fields.
			this.description.setText(obstacle.getDescription());
			this.km.setText(Tools.isUnknown(obstacle.getKm()));
		}else{
			// If the Obstacle does not exist, throw an Exception.
			throw new Exception("Obstacle submitted to Prompt.toPrompt() is null.");
		}
	}
	
	/**
	 * Loads a Branch into the Prompt window for editing.
	 * @param branch The Branch that is to be edited.
	 * @throws Exception
	 */
/*
 * MASTERY FACTOR 6 - Polymorphism. 
 */
	public void toPrompt(Branch branch) throws Exception{
		// Check whether the Branch exists.
		if(branch != null){
			// Save the pointer to the Branch.
			this.branch = branch;
			
			// Set the correct PromptPanels as visible.
			this.setScreen(false);
			
			// Load the Restriction into the corresponding (and now visible) PromptPanel.
			this.single.setPanel(branch.getRestriction());
			
			// Display the Description and kilometer of the Branch in their corresponding fields.
			this.description.setText(branch.getDescription());
			this.km.setText(Tools.isUnknown(branch.getKm()));
		}else{
			// If the Branch does not exist, throw an Exception.
			throw new Exception("Branch submitted to Prompt.toPrompt() is null.");
		}
	}
	
	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		// The user pressed the EXIT button.
		if(e.getSource() == this.exit){
			// Exit the prompt window without saving.
			this.parent.exitPrompt(false);
		}
		
		// The user pressed the SAVE button.
		if(e.getSource() == this.save){
			try{
				// Check whether the kilometer in its text field is of a valid value.
				this.kmDouble = Tools.getDouble(this.km.getText().trim());
				// Remove invalid characters from the description.
				this.description.setText(Tools.removeInvalidCharacters(this.description.getText()));
			}catch(Exception ex){
				// If it is not, inform the user of this error.
				Tools.error("Please enter a numeric value as the kilometer or enter three questionmarks ( ??? ) if it is unknown.",this);
				return;
			}
			this.parent.exitPrompt(true);
		}
	}
	
	/**
	 * Getter for the object variable storing the description of the currently edited Obstacle or Branch.
	 * @return The object variable storing the description of the currently edited Obstacle or Branch.
	 */
	public String getDescription(){
		return this.description.getText();
	}
	
	/**
	 * Getter for the object variable storing the kilometer of the currently edited Obstacle or Branch.
	 * @return The object variable storing the kilometer of the currently edited Obstacle or Branch.
	 */
	public double getKmDouble(){
		return this.kmDouble;
	}
	
	/**
	 * Return the total text used up by the values edited in this Prompt window.
	 * @return The total text used up by the values edited in this Prompt window
	 */
	public String getTotalText(){
		return this.km.getText().trim() + this.description.getText().trim();
	}
}

package dossier;

import javax.swing.JTable;

/**
 * Class that extends a JTable and is to be used when the program creates any tables.
 * It ensures that no cells within the tables will be editable.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class MyTable extends JTable{
	private static final long serialVersionUID = 2166090102266243113L;

	/**
	 * Standard constructor with no different featured
	 */
	public MyTable(){
		super();
	}
	
	/**
	 * Standard constructor with no different featured
	 */
	public MyTable(Object[][] data, Object[] columns){
		super(data,columns);
	}
	
	/**
	 * This method is the whole point of this class. It overrides the method isCellEditable
	 * that is of the JTable and makes it return false for every cell. (No cell will ever)
	 * be editable.
	 */
	public boolean isCellEditable(int row, int column) {
       return false;
    }
}
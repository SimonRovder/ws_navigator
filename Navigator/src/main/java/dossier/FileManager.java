package dossier;

/**
 * Class that manages all writing and reading of both the Random Access Files and the Sequential files.
 * Manages the correct usage of the classes that perform the actual reading and writing.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class FileManager {
	
	// Variable for reading sequential files
	private Reader reader;
	
	// Variable for writing sequential files
	private Append append;
	
	/*
	 * Two variables for reading Random Access Files.
	 * One for Obstacles, one for Branches.
	 */
	private DirectAccessFile direct;
	private DirectAccessFile directB;
	
	// The length of a single record in the Random Access Files.
	public static int recordLength;
	
	/**
	 * Constructor that creates the necessary object variables.
	 */
	public FileManager(){
		// Create the object variables.
		this.reader = new Reader();
		this.append = new Append();
		this.direct = new DirectAccessFile();
		this.directB = new DirectAccessFile();
	}
	
	/**
	 * Deletes the record at the provided index from the file that contains the Obstacles of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be deleted.
	 * @param index The index of the information that is to be deleted in the file.
	 * @throws Exception
	 */
	public void deleteObstacle(String fileName, long index) throws Exception{
		this.delete(fileName + ".OBST", index);
	}
	
	/**
	 * Deletes the record at the provided index from the file that contains the Branches of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName
	 * @param index
	 * @throws Exception
	 */
	public void deleteBranch(String fileName, long index) throws Exception{
		this.delete(fileName + ".BRAN", index);
	}
	
	/**
	 * Deletes the record at the provided index from the file the name of which is provided
	 * in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be deleted.
	 * @param index The index of the information that is to be deleted in the file.
	 * @throws Exception
	 */
	public void delete(String fileName, long index) throws Exception{
		this.direct.openFile(fileName, recordLength); // Open the file
		this.direct.delete(index); // Delete the record at the position provided in the parameters.
		this.direct.close(); // Cloas the file.
	}
	
	/**
	 * Rewrites the record at the provided index from the file that contains the Obstacles of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param text String with the updated information for the Obstacle
	 * @param index
	 * @throws Exception
	 */
	public void updateObstacle(String fileName, String text, long index) throws Exception{
		this.update(fileName + ".OBST", text, index);
	}
	
	/**
	 * Rewrites the record at the provided index in the file that contains the Branches of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param branch String with the updated information for the Branch
	 * @param index The index of the information that is to be edited in the file.
	 * @throws Exception
	 */
	public void updateBranch(String fileName, String text, long index) throws Exception{
		this.update(fileName + ".BRAN", text, index);
	}
	
	/**
	 * Rewrites the record at the provided index in the file the name of which is provided in
	 * the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param text String with the updated content of the record that is to be updated.
	 * @param index The index of the information that is to be edited in the file.
	 * @throws Exception
	 */
	public void update(String fileName, String text, long index) throws Exception{
		this.direct.openFile(fileName, recordLength); // Open the file
		this.direct.writeTo(text, index); // Rewrite the record that is to be edited.
		this.direct.close(); // Close the file.
	}
	
	
	/**
	 * Rewrites the record at the provided index from the file that contains the Obstacles of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param text String with the updated information for the Obstacle
	 * @param index
	 * @throws Exception
	 */
	public void insertObstacle(String fileName, String text, long index) throws Exception{
		this.insert(fileName + ".OBST", text, index);
	}
	
	/**
	 * Rewrites the record at the provided index in the file that contains the Branches of the highway
	 * the name of which is provided in the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param branch String with the updated information for the Branch
	 * @param index The index of the information that is to be edited in the file.
	 * @throws Exception
	 */
	public void insertBranch(String fileName, String text, long index) throws Exception{
		this.insert(fileName + ".BRAN", text, index);
	}
	
	/**
	 * Rewrites the record at the provided index in the file the name of which is provided in
	 * the fileName parameter.
	 * @param fileName Directory of the file containing the information that is to be edited.
	 * @param text String with the updated content of the record that is to be updated.
	 * @param index The index of the information that is to be edited in the file.
	 * @throws Exception
	 */
	public void insert(String fileName, String text, long index) throws Exception{
		this.direct.openFile(fileName, recordLength); // Open the file
		this.direct.insert(text, index); // Insert the record that is to be edited to the correct position.
		this.direct.close(); // Close the file.
	}
	
	/**
	 * Creates a new HighwaySystem and fills it with highways, obstacles and branches read from direct access files
	 * Names of the highways and highway files are found in the HIGHWAYS.BASE file.
	 * @return The new highwaySystem
	 * @throws Exception
	 */
	public HighwaySystem initialize() throws Exception{
		// Create a new HighwaySystem.
		HighwaySystem highwaySystem = new HighwaySystem();
		
		//Open the basic loading file.
		this.reader.openFile("HIGHWAYS.BASE");
		
		/* Read the length of a single random access file record from the backup file and save it.
		 * in the object variable.
		 */
		recordLength = Integer.parseInt(this.reader.nextLine());
		
		// Read the amount of Highways from the second line.
		int limiti = Integer.parseInt(this.reader.nextLine());
		
		int limitj;		// Prepare an Integer for iteration.
		String name;	// Prepare a String for reading.
		
		
		//Repeat the following process as many times, as there were Highways
		for(int i = 0; i < limiti; i++){
			// Read the name of the Next Highway from the loading file.
			name = this.reader.nextLine();
			
			//Create a Highway with that name.
			Highway highway = new Highway(name);
			
			// Open the Random Access File of Obstacles for the current Highway name.
			this.direct.openFile(name + ".OBST", recordLength);
			// Get the amount of records in the file
			limitj = (int) this.direct.getRecordNumber();
			for(int j = 0; j< limitj; j++){
				// Read all the records by iterating and add them all to the Highway's LinkedList of Obstacles.
				highway.addToEnd(Tools.parseObstacle(this.direct.read(j), j));
			}

			// Open the Random Access File of Branches for the current Highway name.
			this.direct.openFile(name + ".BRAN", recordLength);
			// Get the amount of records in the file
			limitj = (int) this.direct.getRecordNumber();
			for(int j = 0; j< limitj; j++){
				// Read all the records by iterating and add them all to the Highway's LinkedList of Branches.
				highway.addToEnd(Tools.parseBranch(this.direct.read(j), j));
			}
			
			// Add the fully loaded Highway to the HighwaySystem.
			highwaySystem.addToEnd(highway);
		}
		// Checks whether the loaded HighwaySystem is valid.
		if(Tools.validateHighwaySystem(highwaySystem)){
			return highwaySystem; // Return the fully loaded HighwaySystem.
		}
		// If it is not valid, throw an Exception.
		throw new Exception("The backup file contains invalid data.");
	}
	
	/**
	 * Loads a backup file at the directory provided in the fileName. Writes the loaded data into both the
	 * HighwaySystem and the random access files.
	 * @param fileName The directory of the backup file thyat is to be loaded.
	 * @return The new HighwaySystem (loaded from the backup).
	 * @throws Exception
	 */
	public HighwaySystem loadBackup(String fileName) throws Exception{
		// Open the sequential backup file.
		this.reader.openAbs(fileName);
		
		// Open the basic loading file
		this.append.open("HIGHWAYS.BASE", false);
		
		/* Read the length of a single random access file record from the backup file and save it
		 * in the object variable.
		 */
		recordLength = Integer.parseInt(this.reader.nextLine());
		
		// Read the amount of Highways from the second line.
		int limiti = Integer.parseInt(this.reader.nextLine());
		
		// Write both the record length and the amount of Highways to the basic loading file.
		this.append.write("" + recordLength);
		this.append.write("" + limiti);
		
		int limitj;		// Prepare an Integer for iteration.
		String name;	// Prepare a String for reading Highway Names.
		String s;		// Prepare a String for reading Obstacles and Branches from the backup file.
		
		//Repeat the following process as many times, as there were Highways
		for(int i = 0; i < limiti; i++){
			// Read the name of the next Highway.
			name = this.reader.nextLine();
			
			// Write it into the basic loading file.
			this.append.write(name);
			
			// Open and empty random access files for the Obstacles and Branches of this highway
			this.direct.openFile(name + ".OBST",recordLength);
			this.direct.resetFile();
			this.directB.openFile(name + ".BRAN", recordLength);
			this.directB.resetFile();
			
			// Read the amount of Branches and Obstacles in the Highway from the backup file.
			limitj = Integer.parseInt(this.reader.nextLine());
			
			// Iterate and read all the Obstacles and Branches of the current Highway from the backup file.
			for(long j = 0; j < limitj; j++){
				// Read the next line (which is either a Branch or Obstacle).
				s = this.reader.nextLine();
				
				// Check whether the length of the record is valid.
				if(Tools.getUTFLength(s) <= recordLength){
					
					// Check whether it is an Obstacle or a Branch
					if(Tools.getType(s).equals("VETVA")){
						// If it is a Branch, write it into the random access file of Branches.
						this.directB.append(s);
					}else{
						// If it is not a Branch, write it into the random access file of Obstacles.
						this.direct.append(s);
					}
				}else{
					// If the record is too long, throw an Exception.
					throw new Exception();
				}
			}
		}
		
		// Once the loading is done, close both the basic loading file and the backup file.
		this.append.close();
		this.reader.close();
		
		// Now initialize and return the initialized HighwaySystem.
		return this.initialize();
	}
	
	/**
	 * Saves a backup of the provided HighwaySystem in a sequential file, the directory of which is
	 * provided in the parameter fileName.
	 * @param fileName The directory of the backup file that is to be created.
	 * @param highwaySystem The current HighwaySystem.
	 * @throws Exception
	 */
	public void saveBackupFile(String fileName, HighwaySystem highwaySystem) throws Exception{
		// Open the new backup file (erase everything that it contains if it already exists).
		this.append.openAbs(fileName, false);
		
		// Write the record length into the new backup file.
		this.append.write(recordLength + "");
		
		// Write the amount of Highways into the new backup file.
		this.append.write(highwaySystem.getHighways() + "");
		
		// Get the first Highway in the HighwaySystem.
		Highway h = (Highway) highwaySystem.getFirst();
		
		//Repeat the following process as many times, as there were Highways
		while(h != null){
			// Get the first Branch and first Obstacle of the current Highway.
			Obstacle o = h.getFirstObstacle();
			Branch b = h.getFirstBranch();
			
			// Write the name of the current Highway into the new backup file.
			this.append.write(h.getName());
			
			// Write the amount of Obstacles and Branches of the current Highway into the new backup file.
			this.append.write((h.getBranches() + h.getObstacles()) + "");
			
			// Iterate, writing each Obstacle into the new backup file.
			while(o != null){
				// Parse the Obstacle as text and write it into the new backup file.
				this.append.write(Tools.parseText(o));
				// Continue iteration with the next Obstacle.
				o = (Obstacle) o.getNext();
			}
			
			// Iterate, writing each Branch into the new backup file.
			while(b != null){
				// Parse the Obstacle as text and write it into the new backup file.
				this.append.write(Tools.parseText(b));
				// Continue iteration with the next Branch.
				b = (Branch) b.getNext();
			}
			// Continue iteration with the next Highway.
			h = (Highway) h.getNext();
		}
		
		// Close the new backup file.
		this.append.close();
	}

	/**
	 * Updates the file HIGHWAYS.BASE from which highway names are initially loaded with names of the
	 * Highways in the provided HighwaySystem.
	 * @param highwaySystem The current HighwaySystem.
	 * @throws Exception
	 */
	public void updateBaseFile(HighwaySystem highwaySystem) throws Exception {
		// Open the basic loading file and erase its content.
		this.append.open("HIGHWAYS.BASE", false);
		// Prepare the first Highway of the HighwaySystem.
		Highway h = (Highway) highwaySystem.getFirst();
		// Write the length of a single Random Access File record in the basic loading file.
		this.append.write(recordLength + "");
		// Write the amount of Highways in the basic loading file.
		this.append.write(highwaySystem.getHighways() + "");
		while(h != null){
			// Iterate through all the Highways, writing their names into the basic loading file.
			this.append.write(h.getName());
			h = (Highway) h.getNext();
		}
		// Close the basic loading file.
		this.append.close();
	}
	
	/**
	 * Deletes all data existing in the highway files for the highway name provided in the fileName
	 * @param highwayName The name of the Highway.
	 * @throws Exception
	 */
	public void nullify(String highwayName) throws Exception{
		/* Open, reset and close both the Branch and the Obstacle random access file corresponding
		 * to the name of the Highway passed in the parameters.
		 */
		this.direct.openFile(highwayName + ".OBST", recordLength);
		this.direct.resetFile();
		this.direct.close();
		this.direct.openFile(highwayName + ".BRAN", recordLength);
		this.direct.resetFile();
		this.direct.close();
	}

	/**
	 * Generates an XML file with the information from the report provided. Prepares the data into a format
	 * that can be exported into PDF format.
	 * @param report that is already filled out.
	 * @param trip with the vehicle information that was used in the assessment
	 * @throws Exception if something goes wrong
	 */
	public void output(Report report, Trip trip) throws Exception {
		// open file where the output prepared for PDF export will be saved
		this.append.openAbs(Tools.fileDataLink + "/input.xml", false);
		
		// open the file there the basic XML structure is saved (like a template)
		this.reader.openAbs(Tools.fileDataLink + "/loader.txt");
		/*
		 * Reads all the lines from the template file and inserts the evaluated information in between
		 * the lines (into the xml tags)
		 */
		this.append.write(this.reader.nextLine());
		this.append.write(trip.getCompany());
		this.append.write(this.reader.nextLine());
		this.append.write(trip.getDate());
		this.append.write(this.reader.nextLine());
		this.append.write(report.normal);
		this.append.write(this.reader.nextLine());
		this.append.write(report.exclusive);
		this.append.write(this.reader.nextLine());
		this.append.write(report.extraordinary);
		this.append.write(this.reader.nextLine());
		this.append.write(report.unknown);
		this.append.write(this.reader.nextLine());
		
		//Close the files
		this.append.close();
		this.reader.close();
	}
	
	/**
	 * Saves an emergency backup of the HighwaySystem passed in the parameters.
	 * @param highwaySystem The HighwaySystem that needs to have a backup made.
	 * @throws Exception
	 */
	public void saveEmergencyBackup(HighwaySystem highwaySystem) throws Exception {
		// Save the backup.
		this.saveBackupFile("EMERGENCYBACKUPFILE.BACKUP", highwaySystem);
	}
}
package dossier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTabbedPane;

/**
 * The class that handles the GUI for editing, adding, deleting and moving of Highways, Branches and
 * Obstacles.
 * 
 * Extends the BasePanel.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class Editor extends BasePanel implements ActionListener{
	private static final long serialVersionUID = 5318197131248269378L;

	/* Pointer to the Guru class that created this class. (Used to call
	 * methods of Guru).
	 */
	private Guru guru;
	
	// The Prompt window into which Obstacles and Branches are loaded for editing.
	private Prompt prompt;

	// The array of EditorPanels - One for each Highway.
	private EditorPanel[] editorPanels;

	// Flag variable for the Highway thje Obstacle or Branch of which is being edited.
	private Highway highway;
	
	// Flag variable for the type of edit taking place (enum value).
	private EditTypes mode;

	// VISUAL COMPONENTS - ELEMENTS OF THE GUI
	private MyButton createHighway;
	private MyButton deleteHighway;
	private MyButton moveUp;
	private MyButton moveDown;
	private MyButton mainMenu;
	private JTabbedPane tabbedPane;
	
	
	/**
	 * Constructor saves the pointer passed in the parameters to the
	 * object variable and creates the GUI along with other necessary classes.
	 * 
	 * @param guru The Guru class that created this class.
	 */
	public Editor(Guru guru){
		super(); // Call to the constructor of the superclass.
		// Save the passed Guru as the object variable.
		this.guru = guru;
		
		// Create the Prompt window, via which Obstacles and Branches will be edited.
		this.prompt = new Prompt(this);
		
		/*
		 * Creating the components of the GUI
		 * and placing them onto the BasePanel
		 */
		this.tabbedPane = new JTabbedPane();
		this.tabbedPane.setBounds(20,60,740,480);
		this.add(this.tabbedPane);
		
		this.createHighway = new MyButton("Create a new Highway",20,20,170,30,this);
		this.add(this.createHighway);
		
		this.deleteHighway = new MyButton("Delete the Highway",210,20,170,30,this);
		this.add(this.deleteHighway);
		
		this.moveDown = new MyButton("LEFT", 400, 20, 80, 30,this);
		this.add(this.moveDown);

		this.moveUp = new MyButton("RIGHT", 490, 20, 80, 30,this);
		this.add(this.moveUp);
		
		this.mainMenu = new MyButton("Main Menu",590,20,170,30,this);
		this.add(this.mainMenu);
		
	}

	/**
	 * Creates new EditorPanels for each Highway and places them onto the JTabbedPane of this screen.
	 */
	public void createPanels(){
		// Remove all potential panels from the JTabbedPane.
		this.tabbedPane.removeAll();
		
		/* 
		 * Create the array of EditorPanels with as many EditorPanels, as there are Highways
		 * in the HighwaySystem of Guru.
		 */
		this.editorPanels = new EditorPanel[this.guru.highwaySystem.getHighways()];
		
		// Get the first Highway.
		Highway temp = (Highway) this.guru.highwaySystem.getFirst();
		int loop = 0; // Set up a loop counter.
		
		// Iterate through all the Highways.
		while(temp != null){
			// Create a new EditorPanel for the current Highway.
			this.editorPanels[loop] = new EditorPanel(temp, this);
			// Add the new EditorPanel to the JTabbedPane.
			this.tabbedPane.add(this.editorPanels[loop], temp.getName());
			// Continue iteration with the next highway and a higher loop count.
			temp = (Highway) temp.getNext();
			loop++;
		}
		// Update the newly created EditorPanels.
		this.updatePanels();
	}
	
	/**
	 * Updates the EditorPanels displayed in the JTabbedPane of this screen.
	 */
	public void updatePanels(){
		for(int i = 0; i < this.editorPanels.length; i++){
			// Iterate through all the EditorPanels and update each one in the process.
			this.editorPanels[i].updatePanels();
		}
	}
	
	/**
	 * Creates a new Highway and adds it to the HighwaySystem of Guru.
	 * As this is a recursive algorithm, the algorithm keeps track of the amount of failed
	 * attempts the user made to enter the name of the new Highway. To prevent a Stack Overflow,
	 * the limit to the amount of failed attempts is set to a reasonable number (20).
	 */
	public boolean addHighway(int attempt){
		// Prompt the user for a name for the new Highway.
		String name = Tools.userInput("Enter the name of the new Highway","");
		// Check what button the user pressed. If the String is null, the user pressed CANCEL.
		if(name != null){
			// Trim the String.
			name = name.trim();
			// Check whether the user provided any text. Only continue, if text was provided.
			if(!name.equals("")){
				// Check whether the provided name is not already taken by a different Highway.
				if(this.guru.highwaySystem.isNameFree(name)){
					// Create a new Highway with the provided name.
					Highway h = new Highway(name);
					// Add the Highway to the HighwaySystem in Guru.
					this.guru.highwaySystem.addToEnd(h);
					try {
						// Update the basic Loading file to account for the new Highway.
						this.guru.fileManager.updateBaseFile(this.guru.highwaySystem);
						// Prepare the Random Access Files for the new Highway.
						this.guru.fileManager.nullify(h.getName());
						// Recreate the EditorPanels to account for the new Highway.
						this.createPanels();
						// Recreate the RoutePlannerPanels to account for the Change.
						this.guru.routePlanner.createPanels();
						// Return true - Highway was added.
						return true;
					} catch (Exception ex) {
						// If any error occured, inform the user and return false.
						Tools.error("An error occured and the highway could not be saved.\n" + ex.toString(),this);
						return false;
					}
				}else{
					/*
					 * If the provided name is already taken by a different Highway, check whether the user
					 * is not out of the range of attempts and have him try again by calling self with a higher
					 * attempt count.
					 */
					if(attempt < 20){
						Tools.error("A highway with that name already exists. Enter a different name.",this);
						return this.addHighway(attempt + 1);
					}
					// If the attempt limit was exceeded, inform the user of this and return false.
					Tools.error("To many failed attempts.", this);
					return false;
				}
			}else{
				/*
				 * If the user failed to provide any text as the new Highway's name, check whether the user
				 * is not out of the range of attempts and have him try again by calling self with a higher
				 * attempt count.
				 */
				if(attempt < 20){
					Tools.error("Please enter a highway name",this);
					return this.addHighway(attempt + 1);
				}
				// If the attempt limit was exceeded, inform the user of this and return false.
				Tools.error("To many failed attempts.", this);
				return false;
			}
		}
		// The user pressed CANCEL, hence do nothing.
		return false;
	}
	
	/**
	 * Deletes a Highway from the HighwaySystem of Guru.
	 */
	private void deleteHighway(){
		// Ask the user to confirm the deletion.
		if(Tools.confirm("Are you sure you wish to delete the selected highway?\nThis process cannot be undone!",this)){
			// Get the selected Highway using the index of the selected tab on the JTabbedPane.
			Highway highway = this.guru.highwaySystem.getHighway(this.tabbedPane.getSelectedIndex());
			// Check whether a Highway was returned.
			if(highway != null){
				try {
					// Delete the Highway From the HighwaySystem of Guru.
					this.guru.highwaySystem.delete(highway);
					// Update the basic loading file to account for the deletion of the Highway. 
					this.guru.fileManager.updateBaseFile(this.guru.highwaySystem);
					// Erase the content of the Random Access Files corresponding to the deleted Highway.
					this.guru.fileManager.nullify(highway.getName());
				} catch (Exception ex) {
					// If an error occurred, inform the user.
					Tools.error("An error has occured and the highway could not be deleted.\n\n" + ex.toString(),this);
				}
				// Recreate the EditorPanels to account for the change.
				this.createPanels();
				// Recreate the RoutePlannerPanels to account for the change.
				this.guru.routePlanner.createPanels();
			}
		}
	}
	
	/**
	 * Loads the provided Obstacle into the Prompt window for editing.
	 * @param highway The Highway the provided Obstacle is on.
	 * @param obstacle The Obstacle that is to be edited.
	 */
	public void edit(Highway highway, Obstacle obstacle){
		try{
			// Load the Obstacle into the Prompt window.
			this.prompt.toPrompt(obstacle);
			// Set the edit mode flag variable to the correct value.
			this.mode = EditTypes.EDIT_OBSTACLE;
			// Save the provided Highway in the object variable.
			this.highway = highway;
			
			// Switch views between the Prompt and Guru windows.
			this.prompt.setVisible(true);
			this.guru.setVisible(false);
		}catch(Exception ex){
			// If an error occurred, inform the user.
			Tools.error("An error occured.\n\n" + ex.toString(),this);
		}
	}
	
	/**
	 * Loads the provided Branch into the Prompt window for editing.
	 * @param highway The Highway the provided Branch is on.
	 * @param branch The Branch that is to be edited.
	 */
	public void edit(Highway highway, Branch branch){
		try{
			// Load the Branch into the Prompt window.
			this.prompt.toPrompt(branch);
			// Set the edit mode flag variable to the correct value.
			this.mode = EditTypes.EDIT_BRANCH;
			// Save the provided Highway in the object variable.
			this.highway = highway;

			// Switch views between the Prompt and Guru windows.
			this.prompt.setVisible(true);
			this.guru.setVisible(false);
		}catch(Exception ex){
			// If an error occurred, inform the user.
			Tools.error("An error occured.\n\n" + ex.toString(),this);
		}
	}
	
	/**
	 * Adds a new Obstacle to the provided index of the provided Highway.
	 * @param highway The Highway that the new Obstacle will belong to.
	 * @param index The index of the new Obstacle on the provided Highway.
	 */
	public void addObstacle(Highway highway, int index){
		try {
			// Prompt the user for the type of the Obstacle
			String s = Tools.select(Tools.options, "MOST", "Choose the obstacle type");
			// Check whether the user made a selection.
			if(s != null){
				// Load a new Obstacle with the correct index to the Prompt window.
				this.prompt.toPrompt(new Obstacle(index,s,-1,"",new Restriction(), new Restriction()));
				// Save the provided Highway in the object variable.
				this.highway = highway;
				// Set the edit mode flag variable to the correct value.
				this.mode = EditTypes.ADD_OBSTACLE_TO;

				// Switch views between the Prompt and Guru windows.
				this.prompt.setVisible(true);
				this.guru.setVisible(false);
			}
			// If the user made no selection (pressed CANCEL), do nothing.
		} catch (Exception ex) {
			// If an error occurred, inform the user.
			Tools.error("An error occured.\n\n" + ex.getMessage(),this);
		}
	}
	
	/**
	 * Adds a new Branch to the provided index of the provided Highway.
	 * @param highway The Highway that the new Branch will belong to.
	 * @param index The index of the new Branch on the provided Highway.
	 */
	
	public void addBranch(Highway highway, int index){
		try {
			// Load a new Branch with the correct index to the Prompt window.
			this.prompt.toPrompt(new Branch(index,"VETVA",-1,"",new Restriction()));
			// Save the provided Highway in the object variable.
			this.highway = highway;
			// Set the edit mode flag variable to the correct value.
			this.mode = EditTypes.ADD_BRANCH_TO;

			// Switch views between the Prompt and Guru windows.
			this.prompt.setVisible(true);
			this.guru.setVisible(false);
			
		} catch (Exception e) {
			// If an error occurred, inform the user.
			Tools.error("An error occured.\n\n" + e.getMessage(),this);
		}
	}
	
	
	/**
	 * Deletes the provided Obstacle from the provided Highway.
	 * @param highway The Highway with the Obstacle that is to be deleted.
	 * @param obstacle The Obstacle that is to be deleted.
	 */
	public void deleteObstacle(Highway highway, Obstacle obstacle){
		// Ask the user to confirm the deletion.
		if(Tools.confirm("Are you sure you wish to delete the selected thing?",this)){
			try{
				// Delete the Obstacle from the Random Access File corresponding to it.
				this.guru.fileManager.deleteObstacle(highway.getName(), obstacle.getIndex());
				// Delete the Obstacle from the Highway.
				highway.delete(obstacle);
				// Update all panels corresponding to the Highway to show the changes.
				this.updatePanelsOf(highway);
				// Inform the user that the deletion is complete.
				Tools.message("Delete complete.",this);
				return;
			}catch(Exception ex){
				// If an error occurred, inform the user.
				Tools.error("An error occured.\n\n" + ex.toString(),this);
			}
		}
	}
	
	/**
	 * Deletes the provided Branch from the provided Highway.
	 * @param highway The Highway with the Branch that is to be deleted.
	 * @param branch The Branch that is to be deleted.
	 */
	public void deleteBranch(Highway highway, Branch branch){
		// Ask the user to confirm the deletion.
		if(Tools.confirm("Are you sure you wish to delete the selected thing?",this)){
			try{
				// Delete the Branch from the Random Access File corresponding to it.
				this.guru.fileManager.deleteBranch(highway.getName(), branch.getIndex());
				// Delete the Branch from the Highway.
				highway.delete(branch);
				// Update all panels corresponding to the Highway to show the changes.
				this.updatePanelsOf(highway);
				// Inform the user that the deletion is complete.
				Tools.message("Delete complete.", this);
				return;
			}catch(Exception ex){
				// If an error occurred, inform the user.
				Tools.error("An error occured.\n\n" + ex.toString(),this);
			}
		}
	}
	
	/**
	 * Handles the editing of the Obstacle or Branch that was loaded into the Prompt window.
	 * (Called then the user is done editing)
	 * 
	 * @param saveChanges (true if the user clicked SAVE in the Prompt window, false if the user clicked CANCEL)
	 */
	public void exitPrompt(boolean saveChanges){
		// Check whether the user clicked SAVE, or CANCEL. proceed with edit analysis only if SAVE was clicked.
		if(saveChanges){
			boolean change = false; // create a flag boolean indicating whether any change was actually made.
			// Prepare Restrictions for loading from the Prompt window.
			Restriction left;
			Restriction right;
			Restriction restriction;
			/*
			 * Switch the editing mode variable. This variable was set when the user entered the Prompt
			 * window and now indicates, what type of change is the user making.
			 */
/*
 * MASTERY FACTOR 11 -- 5 - Complex selection - switch.
 */
			switch(this.mode){
				case EDIT_OBSTACLE: // The user is editing an existing Obstacle
					// Check whether the provided information will fit a record of the Random Access File.
					if( Tools.getUTFLength(this.prompt.left.getTotalText() + this.prompt.right.getTotalText() + this.prompt.getTotalText() + 50) < FileManager.recordLength){
						// If the record will fit, load the two restrictions of the Obstacle from the PromptPanels.
						left = this.prompt.left.getRestriction();
						right = this.prompt.right.getRestriction();
						// Check whether both Restrictions were filled out correctly.
						if(left !=null && right != null){
							// If both Restrictions were filled out correctly, continue.
							try {
								// Re-setup the edited Obstacle using the new information provided by the user.
								this.prompt.obstacle.setup(this.prompt.obstacle.getIndex(),this.prompt.obstacle.getType() ,this.prompt.getKmDouble(), this.prompt.description.getText(), left, right);
								// Update the record in the Random Access File, containing the edited Obstacle.
								this.guru.fileManager.updateObstacle(this.highway.getName(), Tools.parseText(this.prompt.obstacle), this.prompt.obstacle.getIndex());
								// Set the change flag to true.
								change = true;
							} catch (Exception ex) {
								// If an error occurred, inform the user.
								Tools.error("An error occured.\n\n" + ex.toString(),this);
							}
						}else{
							/*
							 * If any of the information provided in the Restrictions is not of valid form,
							 * inform the user of this error.
							 */
							Tools.warning("Please enter only valid values into the fields. Watch out for the following:\n\n- Note that a decimal point is a dot, not a comma.\n- The three weight limits must be in order from smallest value to largest value.",this);
						}
					}else{
						/*
						 * If the total information provided in the Obstacle is too long,
						 * inform the user of this error.
						 */
						Tools.error("The information provided is too long. Please shorten it.", this);
					}
					break;
				case EDIT_BRANCH: // The user is editing an existing Branch
					// Check whether the provided information will fit a record of the Random Access File.
					if( Tools.getUTFLength(this.prompt.single.getTotalText() + this.prompt.getTotalText() + 50) < FileManager.recordLength){
						// If the record will fit, load the restriction of the Branch from the PromptPanels.
						restriction = this.prompt.single.getRestriction();
						// Check whether the Restriction was filled out correctly.
						if(restriction != null){
							// If the Restriction was filled out correctly, continue.
							try {
								// Re-setup the edited Branch using the new information provided by the user.
								this.prompt.branch.setup(this.prompt.branch.getIndex(),this.prompt.branch.getType() ,this.prompt.getKmDouble(), this.prompt.description.getText(), restriction);
								// Update the record in the Random Access File, containing the edited Branch.
								this.guru.fileManager.updateBranch(this.highway.getName(), Tools.parseText(this.prompt.branch), this.prompt.branch.getIndex());
								// Set the change flag to true.
								change = true;
							} catch (Exception ex) {
								// If an error occurred, inform the user.
								Tools.error("An error occured.\n\n" + ex.toString(),this);
							}
						}else{
							/*
							 * If any of the information provided in the Restriction is not of valid form,
							 * inform the user of this error.
							 */
							Tools.warning("Please enter only valid values into the fields. Watch out for the following:\n\n- Note that a decimal point is a dot, not a comma.\n- The three weight limits must be in order from smallest value to largest value.",this);
						}
					}else{
						/*
						 * If the total information provided in the Branch is too long,
						 * inform the user of this error.
						 */
						Tools.error("The information provided is too long. Please shorten it.", this);
					}
					break;
				case ADD_OBSTACLE_TO: // The user is adding an Obstacle to a Highway
					// Check whether the provided information will fit a record of the Random Access File.
					if( Tools.getUTFLength(this.prompt.left.getTotalText() + this.prompt.right.getTotalText() + this.prompt.getTotalText() + 50) < FileManager.recordLength){
						// Prepare Restrictions for loading from the Prompt window.
						left = this.prompt.left.getRestriction();
						right = this.prompt.right.getRestriction();
						// Check whether both Restrictions were filled out correctly.
						if(left !=null && right != null){
							// If both Restrictions were filled out correctly, continue.
							try {
								// Re-setup the new Obstacle using the new information provided by the user.
								this.prompt.obstacle.setup(this.prompt.obstacle.getIndex(), this.prompt.obstacle.getType() ,this.prompt.getKmDouble(), this.prompt.description.getText(), left, right);
								// Add the new Obstacle to the Correct position on the Highway.
								this.highway.addToPos(this.prompt.obstacle, (int)this.prompt.obstacle.getIndex());
								// Insert the record onto the correct position in the Random Access File.
								this.guru.fileManager.insertObstacle(this.highway.getName(), Tools.parseText(this.prompt.obstacle), this.prompt.obstacle.getIndex());
								// Set the change flag to true.
								change = true;
							} catch (Exception ex) {
								// If an error occurred, inform the user.
								Tools.error("An error occured.\n\n" + ex.toString(),this);
							}
						}else{
							/*
							 * If any of the information provided in the Restrictions is not of valid form,
							 * inform the user of this error.
							 */
							Tools.warning("Please enter only valid values into the fields. Watch out for the following:\n\n- Note that a decimal point is a dot, not a comma.\n- The three weight limits must be in order from smallest value to largest value.",this);
						}
					}else{
						/*
						 * If the total information provided in the Obstacle is too long,
						 * inform the user of this error.
						 */
						Tools.error("The information provided is too long. Please shorten it.", this);
					}
					break;
				case ADD_BRANCH_TO: // The user is adding an Branch to a Highway
					// Check whether the provided information will fit a record of the Random Access File.
					if( Tools.getUTFLength(this.prompt.single.getTotalText() + this.prompt.getTotalText() + 50) < FileManager.recordLength){
						// If the record will fit, load the restriction of the Branch from the PromptPanels.
						left = this.prompt.single.getRestriction();
						// Check whether the Restriction was filled out correctly.
						if(left !=null){
							// If the Restriction was filled out correctly, continue.
							try {
								// Set up the new Branch using the new information provided by the user.
								this.prompt.branch.setup(this.prompt.branch.getIndex(),"VETVA" ,this.prompt.getKmDouble(), this.prompt.description.getText(), left);
								// Add the new Branch to the Correct position on the Highway.
								this.highway.addToPos(this.prompt.branch, (int)this.prompt.branch.getIndex());
								// Insert the record onto the correct position in the Random Access File.
								this.guru.fileManager.insertBranch(this.highway.getName(), Tools.parseText(this.prompt.branch), this.prompt.branch.getIndex());
								// Set the change flag to true.
								change = true;
							} catch (Exception ex) {
								// If an error occurred, inform the user.
								Tools.error("An error occured.\n\n" + ex.toString(),this);
							}
						}else{
							/*
							 * If any of the information provided in the Restriction is not of valid form,
							 * inform the user of this error.
							 */
							Tools.warning("Please enter only valid values into the fields.Watch out for the following:\n\n- Note that a decimal point is a dot, not a comma.\n- The three weight limits must be in order from smallest value to largest value.",this);
						}
						break;
					}else{
						/*
						 * If the total information provided in the Branch is too long,
						 * inform the user of this error.
						 */
						Tools.error("The information provided is too long. Please shorten it.", this);
					}
			}
			if(change){
				this.updatePanelsOf(this.highway);
				this.prompt.setVisible(false);
				this.guru.setVisible(true);
			}
		}else{
			this.prompt.setVisible(false);
			this.guru.setVisible(true);
		}
	}
	
	/**
	 * Updates the EditorPanel and the RoutePlannerPanel of the provided Highway.
	 * @param highway
	 */
	private void updatePanelsOf(Highway highway){
		// Update the Highway's EditorPanel.
		highway.getEditorPanel().updatePanels();
		// Update the Highways RoutePlannerPanel.
		highway.getRoutePlannerPanel().updatePanel();
	}
	
	/**
	 * The action listener.
	 */
	public void actionPerformed(ActionEvent e) {
		 
		if(e.getSource() == this.mainMenu){
			// The user pressed the main menu button - switch the screen to the main menu.
			this.guru.setScreen(Screens.MAIN_MENU);
		}
		
		if(e.getSource() == this.createHighway){
			// The user pressed the create Highway button. Try to create a Highway and check if it was created.
			if(this.addHighway(0)){
				// If a Highway was created, set the TabbedPane to the Tab of the new Highway (user-friendliness).
				this.tabbedPane.setSelectedIndex(this.guru.highwaySystem.getHighways() - 1);
			}
		}
		
		if(e.getSource() == this.deleteHighway){
			// The user pressed the delete Highway button - Delete the Highway.
			this.deleteHighway();
		}
		
		if(e.getSource() == this.moveUp){
			// The user pressed the move up button.
			
			int num = this.tabbedPane.getSelectedIndex(); // Get the index of the selected tab in the JTabbedPane.
			
			if(num < this.guru.highwaySystem.getHighways() - 1){
				/*
				 * If the selected Highway is not the rightmost Highway, swap it with the Highway that has an
				 * index one greater than the index of the selected Highway.
				 */
				this.swap(this.guru.highwaySystem.getHighway(num), num, this.guru.highwaySystem.getHighway(num + 1), num + 1);
				// Switch the JTabbedPane view to show the moved Highway (user-friendliness).
				this.tabbedPane.setSelectedIndex(num + 1);
			}else{
				/*
				 * If the selected Highway is already the rightmost Highway, it cannot be moved any more to the
				 * right. Inform the user of this error.
				 */
				Tools.error("Incorrect highway selected for this operation.", this);
			}
		}
		
		if(e.getSource() == this.moveDown){
			// The user pressed the move down button.
			
			int num = this.tabbedPane.getSelectedIndex(); // Get the index of the selected tab in the JTabbedPane.
			if(num > 0){
				/*
				 * If the selected Highway is not the leftmost Highway, swap it with the Highway that has an
				 * index one greater than the index of the selected Highway.
				 */
				this.swap(this.guru.highwaySystem.getHighway(num - 1), num - 1, this.guru.highwaySystem.getHighway(num), num);
				this.tabbedPane.setSelectedIndex(num - 1);
			}else{
				/*
				 * If the selected Highway is already the leftmost Highway, it cannot be moved any more to the
				 * left. Inform the user of this error.
				 */
				Tools.error("Incorrect highway selected for this operation.", this);
			}
		}
	}

	/**
	 * Swaps the two provided Obstacles on the provided Highway.
	 * @param o1 First Obstacle.
	 * @param o2 Second Obstacle.
	 * @param highway The Highway on which both the Obstacles are.
	 */
	public void swap(Obstacle o1, Obstacle o2, Highway highway) {
		// Get the indexes of the two Obstacles.
		int i1 = o1.getIndex();
		int i2 = o2.getIndex();
		try{
			// Delete the Obstacles from their Highway.
			highway.delete(o1);
			highway.delete(o2);
			
			// Swap the indexes of the two Obstacles.
			o1.setIndex(i2);
			o2.setIndex(i1);
			
			// Insert the Obstacles back onto the Highway, but at the new position.
			highway.addToPos(o2, i1);
			highway.addToPos(o1, i2);
			
			// Update the records of both the Obstacles in their corresponding Random Access File.
			this.guru.fileManager.updateObstacle(highway.getName(), Tools.parseText(o2), i1);
			this.guru.fileManager.updateObstacle(highway.getName(), Tools.parseText(o1), i2);
			
			// Update the panels of the Highway.
			this.updatePanelsOf(highway);
		}catch(Exception ex){
			// If an error occurred, inform the user.
			Tools.error("An error occured while moving the obstacle.\n\n" + ex.toString(), this);
		}
	}

	/**
	 * Swaps the two provided Branches on the provided Highway.
	 * 
	 * @param b1 First Branch
	 * @param b2 Second Branch
	 * @param highway The Highway on which both the Branches are.
	 */
	public void swap(Branch b1, Branch b2, Highway highway) {
		// Get the indexes of the two Branches.
		int i1 = b1.getIndex();
		int i2 = b2.getIndex();
		try{
			// Delete the Branches from their Highway.
			highway.delete(b1);
			highway.delete(b2);
			
			// Swap the indexes of the two Branches.
			b1.setIndex(i2);
			b2.setIndex(i1);
			
			// Insert the Branches back onto the Highway, but at the new position.
			highway.addToPos(b2, i1);
			highway.addToPos(b1, i2);
			
			// Update the records of both the Branches in their corresponding Random Access File.
			this.guru.fileManager.updateBranch(highway.getName(), Tools.parseText(b2), i1);
			this.guru.fileManager.updateBranch(highway.getName(), Tools.parseText(b1), i2);
			
			// Update the panels of the Highway.
			this.updatePanelsOf(highway);
		}catch(Exception ex){
			// If an error occurred, inform the user.
			Tools.error("An error occured while moving the obstacle.\n\n" + ex.toString(), this);
		}
	}
	
	/**
	 * Swaps the two provided Highways of the HighwaySystem of Guru.
	 * @param h1 First Highway.
	 * @param i1 Index of the first Highway.
	 * @param h2 Second Highway.
	 * @param i2Index of the Second Highway.
	 */
	public void swap(Highway h1, int i1, Highway h2, int i2) {
		try{
			// Delete the Highways from their HighwaySystem.
			this.guru.highwaySystem.delete(h1);
			this.guru.highwaySystem.delete(h2);
			
			// Insert the Highways back onto the HighwaySystem, but at the new position.
			this.guru.highwaySystem.addToPos(h2, i1);
			this.guru.highwaySystem.addToPos(h1, i2);
			
			// Update the basic loading file to account for the changes.
			this.guru.fileManager.updateBaseFile(this.guru.highwaySystem);
			
			// Recreate the EditorPanels and the RoutePlannerPanels to account for the changes.
			this.createPanels();
			this.guru.routePlanner.createPanels();
		}catch(Exception ex){
			Tools.error("An error occured while moving the highway\n\n" + ex.toString(), this);
		}
	}
}
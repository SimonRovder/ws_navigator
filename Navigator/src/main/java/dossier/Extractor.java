package dossier;

public class Extractor{
	
   public static void main(String args[]) throws Exception{
       String[] left = new String[8];
       String[] right = new String[8];
       
       int records = 0;
       
       boolean recordcounter = true;
       boolean appender = true;
       boolean bodkociarka = true;
       
       Reader reader = new Reader();
       reader.openFile("D3.txt");
       Append append = new Append();
       if(appender){
           append.open("D3BACKUP3.txt",false);
           append.write("D3");
           append.write("36");
       }
       
       
       for(int i = 0; i < 50; i++){
           for(int j = 0; j < 8; j++){
               left[j] = reader.nextLine().trim();
               if(bodkociarka){
                   while(left[j].indexOf(";") != -1){
                       System.out.println(left[j]);
                       left[j] = left[j].replace(";","");
                       System.out.println(left[j]);
                   }
               }
           }
           for(int j = 0; j < 8; j++){
               right[j] = reader.nextLine().trim();
               if(bodkociarka){
                   while(right[j].indexOf(";") != -1){
                       System.out.println(right[j]);
                       right[j] = right[j].replace(";","");
                       System.out.println(right[j]);
                   }
               }
           }
           
           if(!left[1].equals("") && !left[1].equals("rezerva") && !right[1].equals("") && !right[1].equals("rezerva")){
                   if(appender){
                       append.write("MOST;-1;" + left[2] + " " + right[2] + ";" + left[1] + ";" + checkVal(left[3]) + ";;" + checkVal(left[5]) + ";" + checkVal(left[6]) + ";" + checkVal(left[7]) + ";" + right[1] + ";" + checkVal(right[3]) + ";;" + checkVal(right[5]) + ";" + checkVal(right[6]) + ";" + checkVal(right[7]) + ";" + "1010");
                   }
                   if(recordcounter){
                       records++;
                   }
           }else{
               if(!left[1].equals("") && !left[1].equals("rezerva")){
                   if(appender){
                       append.write("MOST;-1;" + left[2] + " " + right[2] + ";" + left[1] + ";" + checkVal(left[3]) + ";;" + checkVal(left[5]) + ";" + checkVal(left[6]) + ";" + checkVal(left[7]) + ";" + right[1] + ";;;;;;" + "1000");
                   }
                   if(recordcounter){
                       records++;
                   }
               }
               if(!right[1].equals("") && !right[1].equals("rezerva")){
                   if(appender){
                       append.write("MOST;-1;" + left[2] + " " + right[2] + ";" + left[1] + ";;;;;;" + right[1] + ";" + checkVal(right[3]) + ";;" + checkVal(right[5]) + ";" + checkVal(right[6]) + ";" + checkVal(right[7]) + ";" + "1010");
                   }
                   if(recordcounter){
                       records++;
                   }
               }
           }
           
           if(left[0].indexOf("NAD") != -1 && right[0].indexOf("NAD") != -1){
               if(appender){
                   append.write("NAD;-1;" + left[2] + " " + right[2] + ";" + getNadId(left[0]) + ";;" + checkVal(left[4]) + ";;;;" + getNadId(right[0]) + ";;" + checkVal(right[4]) + ";;;;" + "0101");
               }
               if(recordcounter){
                   records++;
               }
           }else{
               if(left[0].indexOf("NAD") != -1){
                   if(appender){
                       append.write("NAD;-1;" + left[2] + " " + right[2] + ";" + getNadId(left[0]) + ";;" + checkVal(left[4]) + ";;;;;;;;;;" + "0100");
                   }
                   if(recordcounter){
                       records++;
                   }
               }
               if(right[0].indexOf("NAD") != -1){
                   if(appender){
                       append.write("NAD;-1;" + left[2] + " " + right[2] + ";;;;;;;" + getNadId(right[0]) + ";;" + checkVal(right[4]) + ";;;;" + "0001");
                   }
                   if(recordcounter){
                       records++;
                   }
               }
           }
           
           if(left[0].indexOf("VETVA") != -1){
               if(appender){
                   append.write("VETVA;-1;" + left[2] + right[2] + ";"+ getVetvaId(left[0]) + ";" + checkVal(left[3]) + ";;" + checkVal(left[5]) + ";" + checkVal(left[6]) + ";" + checkVal(left[7]) + ";" + "10");
               }
               if(recordcounter){
                   records++;
               }
           }
           
           if(right[0].indexOf("VETVA") != -1){
               if(appender){
                   append.write("VETVA;-1;" + left[2] + right[2] + ";"+ getVetvaId(right[0]) + ";" + checkVal(right[3]) + ";;" + checkVal(right[5]) + ";" + checkVal(right[6]) + ";" + checkVal(right[7]) + ";" + "10");
               }
               if(recordcounter){
                   records++;
               }
           }
           
       }
       if(recordcounter){        
           System.out.println("" + records);
       }
       if(appender){
           append.close();
       }
   }
   
   private static String checkVal(String s){
	   if(s.equals("")){
		   return "-1";		   
	   }else{
		   return s;
	   }
   }
   
   private static String getNadId(String s){
       String[] a;
       a = s.split(" ");
       String id;
       if(a.length != 2){
           id = a[0] + a[1]+ a[2];
       }else{
           id = a[0];
       }
       return id;
   }
   
   private static String getVetvaId(String s){
       String[] a = s.split(" ");
       String id;
       if(a.length != 6){
           id = (a[0]);
       }else{
           id = (a[0] + a[1]+ a[2]);
       }
       return id;
   }
}

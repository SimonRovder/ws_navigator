package dossier;


/**
 * Class stores all information about the transport taking place. That is:
 * - Date of the transport
 * - Company name
 * - Weights of the axles of the vehicle
 * - Separation between the axles of the vehicle
 * - Height of the vehicle
 * - In the LinkedList that it extends it stores:
 *    - The Routes the vehicle will take
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 */
public class Trip extends LinkedList{
	
	// String that stores the name of the company
    private String company;
    
    // String that stores the date of the transport
	private String date;
	
	// Double that stores the total weight of the vehicle
    private double totalWeight;
    // Array of doubles that stores the weights of the individual axles in order.
    private double[] weight;
    
    // Double that stores the total length of the vehicle
    private double totalSeparation;
    // Array of doubles that stores the individual separations between the axles in order.
    private double[] separation;
    
    // Double that stores the height of the vehicle.
    private double height;
    
    
    /**
     * Simple constructor. Only calls the constructor of the super class.
     */
    public Trip(){
    	super(); // Call the constructor of the superclass.
    }
    
    /**
     * Saved the main information about the transport taking place.
     * (Saves the parameters as the corresponding object variables)
     * 
     * @param weight
     * @param separation
     * @param totalWeight
     * @param totalSeparation
     * @param height
     * @param date
     * @param company
     */
    public void setup(double[] weight, double[] separation, double totalWeight, double totalSeparation, double height, String date, String company){
    	// Save each parameter as the object variable.
    	this.company = company;
    	this.date = date;
    	this.weight = weight;
    	this.totalWeight = totalWeight;
    	this.separation = separation;
    	this.totalSeparation = totalSeparation;
    	this.height = height;
    }
    
    /**
     * Adds the provided route to the end of the LinkdeList
     * @param route Route that is to be added
     */
    public void addRoute(Route route){
		this.addToEnd(route); // Call superclass method that adds the Route to the end of the LinkedList
    }
    
    /**
     * Returns the route at the provided index.
     * @param index Index of the required Route in the LinkedList
     * @return The Route at the provided index in the LinkedList
     */
    public Route getRoute(int index){
		return (Route) this.getNode(index); // Call superclass method that finds returns the required Route.
	}
	
    /**
     * Getter for the object variable storing the amount of Routes
     * @return The amount of routes.
     */
	public int getRoutes(){
		return this.amount;
	}
	
	/**
	 * Getter for the object variable storing the company name
	 * @return The company name.
	 */
    public String getCompany() {
		return company;
	}
    
    /**
     * Getter for the object variable storing the date of transport
     * @return The date of trnasport.
     */
	public String getDate() {
		return date;
	}
	
	/**
	 * Getter for the object variable storing the total weight of the vehicle
	 * @return The total weight of the vehicle
	 */
	public double getTotalWeight() {
		return totalWeight;
	}
	
	/**
	 * Getter for the object variable storing the individual weights of the axles
	 * @return The individual weights of the axles
	 */
	public double[] getWeight() {
		return weight;
	}
	
	/**
	 * Getter for the object variable storing the total length of the vehicle
	 * @return The total length of the vehicle.
	 */
	public double getTotalSeparation() {
		return totalSeparation;
	}
	
	/**
	 * Getter for the object variable storing the individual spaces between the axles of the vehicle
	 * @return The individual spaces between the axles of the vehicle.
	 */
	public double[] getSeparation() {
		return separation;
	}
	
	/**
	 * Getter for the object variable storing the total height of the vehicle.
	 * @return The total height of the vehicle.
	 */
	public double getHeight(){
		return this.height;
	}
	
	/**
	 * Erase all information stored in the Trip
	 */
	public void nullify() {
		// Call the function of the superclass that erases all content of the LinkedList.
		super.nullify();
		
		// Erase all content from the object variables.
		this.company = "";
		this.date = "";
		this.totalSeparation = 0;
		this.totalWeight = 0;
		this.weight = null;
		this.separation = null;
		this.height = 0;
	}
}
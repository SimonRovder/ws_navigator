package dossier;

import javax.swing.JLabel;

/**
 * This class has the purpose of making JLabel creation easier. It extends a JLabel
 * and provides a more efficient constructor.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class MyLabel extends JLabel{
	private static final long serialVersionUID = -8237982239859726274L;

	/**
	  * Constructor that sets up the JLabel
	 * @param text Text to be written on the JLabel
	 * @param xPos X Position of the JLabel
	 * @param yPos Y Position of the JLabel
	 * @param width Width of the JLabel
	 * @param height Height of the JLabel
	 */
	public MyLabel(String text, int xPos, int yPos, int width, int height){
		super(text); // Call to the Constructor of the super class, passing the text for the JLabel.
		this.setBounds(xPos,yPos,width,height);	// Setting the Bounds of the JLabel to those provided.
		this.setVisible(true);	
		this.setToolTipText(text); // Making the JLabel visible.
	}
}